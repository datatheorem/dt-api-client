Data Theorem Portal API
-----------------------

This repository provides a reference client in Python for querying the Data Theorem APIs.

The client has been tested on Python 3.7+. Documentation for these APIs is
available at <https://docs.securetheorem.com/> (Formally <https://datatheorem.github.io/PortalApi/>)


### Functional Tests

Requirements :

    XCode should be at least 10.2 ( to support Swift 5.0)
    Swiftlint is required to compile duckduckgo ( brew install swiftlint)

To download the functional tests XCode projects you need to install lfs

    brew install git-lfs
    git lfs install --local  # On your local repo
    git lfs fetch --all  # On your local repo
    git lfs pull  # On your local repo

Then you can launch functional tests with python

    python -m functional_tests.test

The full stacktrace of the building process can be found on file building-stack-trace.txt

### Setup

The Python module uses the _requests_ library for connecting to the Data Theorem APIs:

    python setup.py install

### Results API

The **Results API v2** provides access to a list of all mobile apps registered within a specified customer's Data
Theorem account and the list of scans and security issues found during the scanning of the apps.

The _results_api_cli.py_ sample script can be used to write all the issues affecting all the mobile Apps configured
within your Data Theorem account to a CSV file, with a CVSS score generated for each issue.

A **Result API key** is needed in order to call the script; the API key can be found in the Data Theorem portal at <https://www.securetheorem.com/devsecops/v2/results_api_access>. With the Results API key, the script can
then be used:

    python results_api_cli.py --api-key <results_api_key>

Full documentation for the Results API is available at <https://docs.securetheorem.com/mobile_security_results/introduction.html>.

### Upload API

The **Upload API** can be used to upload **PreProd** mobile binaries directly to Data Theorem for scanning.

The _upload_app_cli.py_ sample script can be used to upload a mobile App build to be scanned.

An **Upload API key** is needed in order to call the script; the API key can be found in the Data Theorem portal at <https://www.securetheorem.com/devsecops/v2/scancicd>. With the Upload API key, the
script can then be used:

    python upload_app_cli.py --api-key <upoad_api_key> --comments test --username user --password p4ss ./app.ipa


Full documentation for the Upload API is available at <https://docs.securetheorem.com/mobile_security_devops/uploading_mobile_apps.html>.

### Upload UI Test API

The **UI Test Upload API** can be used to upload `XCUITest` bundles to be used to perform a fully-automated dynamic scan of its associated mobile app in your Data Theorem account.

The _upload_xcuitest_cli.py_ sample script can be used to automatically extract your application's UI Tests, an `.xctest` bundle, from your Xcode project. This bundle can then be uploaded to DataTheorem and used to quickly dynamically scan the associated app.

An **Upload API key** is needed in order to call the script; the API key can be found in the Data Theorem portal at
https://www.securetheorem.com/sdlc/api under the **"Uploading Applications"** section. With the Upload API key, the
script can then be used:

    python upload_xcuitest_cli.py --api-key <upoad_api_key> MyApp/MyApp.xcodeproj

Optional arguments:

* `--scheme`: Scheme to use to build the Xcode project. Required if more than one scheme exists in the Xcode project.
* `--product`: Product name of to your UI Test target in your Xcode project. Required if you have more than one UI Test target.
* `--release_type`: The type of release this UI Testing bundle should run for: APP_STORE (default) or PRE_PROD.
* `--app_bundle_id`: Specify a bundle ID instead of using the bundle ID from the Xcode project. It will be used to associate the UI Test with the corresponding app in your Data Theorem account.
* `--app_version`: Specify the app version instead of version from the Xcode project. It will be used to associate the UI Test with the corresponding app in your Data Theorem account.
* `--workspace_path`: Extract the testing bundle from a workspace - required if the main Xcode product depends on other Xcode projects within such workspace.

Full documentation for the Upload UI Test API is available at <https://docs.securetheorem.com/mobile_security_devops/uploading_xcuitest_bundles.html>.
