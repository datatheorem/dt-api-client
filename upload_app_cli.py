# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import sys

from argparse import ArgumentParser
from datatheorem_api_client.upload_client import UploadApiClient
from requests.exceptions import HTTPError


def main() -> None:
    parser = ArgumentParser(description="Upload API CLI Tool")
    parser.add_argument(
        "PATH",
        action="store",
        help="Path to the App to upload (.apk, .ipa, .xap or. appx).",
    )
    parser.add_argument("--api-key", help="Your Upload API key.")

    # Optional arguments
    parser.add_argument("--comments", help="Optional comments.")
    parser.add_argument("--username", help="Optional username for dynamic scanning.")
    parser.add_argument("--password", help="Optional password for dynamic scanning.")
    parser.add_argument(
        "--sourcemap",
        help="Optional sourcemap file for de-obfuscating Android scan results.",
    )

    args = parser.parse_args()

    # Validate arguments
    api_key = args.api_key
    if not api_key:
        sys.exit("Upload API key missing; set it using `--api-key`")

    if not os.path.exists(args.PATH):
        sys.exit('No file found at "{0}"'.format(args.PATH))

    if args.sourcemap and not os.path.exists(args.sourcemap):
        sys.exit('No Android mapping file found at "{0}"'.format(args.sourcemap))

    # Upload the build
    try:
        client = UploadApiClient(api_key)

        if args.sourcemap:
            client.upload(
                open(args.PATH, "rb"),
                sourcemap=open(args.sourcemap, "rb"),
                comments=args.comments,
                username=args.username,
                password=args.password,
            )
        else:
            client.upload(
                open(args.PATH, "rb"),
                comments=args.comments,
                username=args.username,
                password=args.password,
            )
    except HTTPError as e:
        # To catch HTTP error, we have to catch `requests.exceptions.HTTPError`.
        error = e.response.json()
        print('Upload failed with error code "{status}": {error}'.format(**error))
        return
    except Exception:
        raise

    print("Build successfully uploaded; results will be published shortly.")


if __name__ == "__main__":
    main()
