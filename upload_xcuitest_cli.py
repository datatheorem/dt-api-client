# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import sys
from argparse import ArgumentParser

from requests.exceptions import HTTPError

from datatheorem_api_client.upload_client import UploadApiClient
from datatheorem_api_client.xcuitest_utils import XCUITestBuilder


def main() -> None:
    parser = ArgumentParser(description="Upload UI Testing bundle CLI Tool")
    parser.add_argument(
        "PATH",
        action="store",
        help="Path to the Xcode project containing the UI Tests to upload",
    )
    parser.add_argument("--api-key", help="Your Upload API key.")

    # Optional arguments
    parser.add_argument(
        "--scheme",
        help="Scheme to use to build the Xcode project. Required if more than one scheme exists in the "
        "Xcode project.",
    )
    parser.add_argument(
        "--workspace_path",
        help="Extract the testing bundle from a workspace - required if the main Xcode product depends "
        "on other Xcode projects within such workspace.",
    )
    parser.add_argument(
        "--product",
        help="Product name of to your UI Test target in your Xcode project. Required if "
        "you have more than one UI Test target.",
    )
    parser.add_argument(
        "--release_type",
        action="store",
        default="APP_STORE",
        choices=["APP_STORE", "PRE_PROD"],
        help="The type of release this UI Testing bundle should run for: APP_STORE (default) or "
        "PRE_PROD.",
    )
    parser.add_argument(
        "--app_bundle_id",
        default=None,
        help="Specify a bundle ID instead of using the bundle ID from the Xcode project. "
        "It will be used to associate the UI Test with the corresponding app in your Data "
        "Theorem account.",
    )
    parser.add_argument(
        "--app_version",
        default=None,
        help="Specify the app version instead of version from the Xcode project. It will be used to "
        "associate the UI Test with the corresponding app in your Data Theorem account.",
    )

    args = parser.parse_args()

    # Validate arguments
    api_key = args.api_key
    if not api_key:
        sys.exit("Upload API key missing; set it using `--api-key`")

    if not os.path.exists(args.PATH):
        sys.exit('No file found at "{0}"'.format(args.PATH))

    # Upload the build
    try:
        client = UploadApiClient(api_key)

        if args.workspace_path:
            helper = XCUITestBuilder.create_from_xcworkspace(
                args.PATH, args.workspace_path
            )
        else:
            helper = XCUITestBuilder(args.PATH)

        print("Building Xcode project and extracting UI testing bundle...")
        file_to_upload, app_bundle_id, app_version = helper.build_xctest(
            args.product, args.scheme
        )

        # If user specified the app's bundle id manually, use that one
        if args.app_bundle_id:
            app_bundle_id = args.app_bundle_id

        # If user specified the app's version manually, use that one
        if args.app_version:
            app_version = args.app_version

        # Perform the upload
        print(
            "Uplading UI Testing bundle for app {} - v{}...".format(
                app_bundle_id, app_version
            )
        )
        client.upload_xcuitest_attachment(
            open(file_to_upload, "rb"),
            bundle_id=app_bundle_id,
            version=app_version,
            release_type=args.release_type,
        )

    except HTTPError as e:
        # To catch HTTP error, we have to catch `requests.exceptions.HTTPError`.
        error = e.response.json()
        print('Upload failed with error code "{status}": {error}'.format(**error))
        return
    except Exception:
        raise

    print("UI Testing bundle successfully uploaded")


if __name__ == "__main__":
    main()
