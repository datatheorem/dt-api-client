import os

from setuptools import find_packages, setup

here = os.path.abspath(os.path.dirname(__file__))
about = {}  # type: dict
with open(os.path.join(here, "datatheorem_api_client", "__version__.py"), "r") as f:
    exec(f.read(), about)

setup(
    name="datatheorem_api_client",
    version=about["__version__"],
    author=about["__author__"],
    description="Reference client for the Data Theorem API",
    url="https://bitbucket.org/datatheorem/dt-api-client",
    packages=find_packages(exclude=("tests",)),
    python_requires=">=3.7",
    install_requires=[
        "requests",
        "typing-extensions",
    ],
)
