# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import csv
import sys
from argparse import ArgumentParser

from datatheorem_api_client.results_api.client import ResultsAPIClient

if sys.version_info[0] < 3:
    IS_PY_2 = True
else:
    IS_PY_2 = False


def main() -> None:
    parser = ArgumentParser(description="Results API v2 CLI tool to export all Mobile Protect apps to a CSV file.")
    parser.add_argument("--api-key", required=True, help="Your Results API key.")
    args = parser.parse_args()

    api_key = args.api_key

    # Initialize the API client
    api_client = ResultsAPIClient(api_key)

    # Fetch all the apps
    print("Fetching Mobile Apps...")
    mobile_apps, next_cursor = api_client.list_mobile_apps()
    # Keep on fetching while we have more results
    while next_cursor:
        print("Fetching Mobile Apps...")
        next_apps, next_cursor = api_client.list_mobile_apps(cursor=next_cursor)
        mobile_apps.extend(next_apps)

    # Fetch all the SDKs
    print("Fetching Third-party SDKs...")
    mobile_app_sdks, next_cursor = api_client.list_mobile_app_sdks()
    # Keep on fetching while we have more results
    while next_cursor:
        print("Fetching Third-party SDKs...")
        next_apps, next_cursor = api_client.list_mobile_app_sdks(cursor=next_cursor)
        mobile_app_sdks.extend(next_apps)

    # Prepare dictionaries
    mobile_apps_dict = {app.id: app for app in mobile_apps}
    sdks_per_app_dict = {app.id: [sdk for sdk in mobile_app_sdks if sdk.mobile_app_id == app.id] for app in mobile_apps}

    # Write everything to a CSV file
    output_filename = "output.csv"
    print("Writing CSV to {}....".format(output_filename))
    with open(output_filename, "w") as csvfile:
        fields = [
            "App Name",
            "App Platform",
            "App Bundle ID",
            "Release Type",
            "Embeds Mobile Protect",
        ]
        writer = csv.writer(csvfile)
        if IS_PY_2:
            fields = [f.encode("utf-8") for f in fields]
        writer.writerow(fields)

        for app_id, mobile_app in mobile_apps_dict.items():
            # Retrieve the corresponding third party SDKs
            mobile_app_sdks = sdks_per_app_dict.get(app_id, [])
            embeds_sdk = any(
                mobile_app_sdk for mobile_app_sdk in mobile_app_sdks if mobile_app_sdk.title == "Mobile Protect"
            )

            row = [
                mobile_app.name,
                mobile_app.platform,
                mobile_app.bundle_id,
                mobile_app.release_type,
                str(embeds_sdk),
            ]

            if IS_PY_2:
                row = [r.encode("utf-8") for r in row]
            writer.writerow(row)

    print("All done... all apps were written to ./{}....".format(output_filename))


if __name__ == "__main__":
    main()
