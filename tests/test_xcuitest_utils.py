import os
import shutil
import tempfile
import unittest

from datatheorem_api_client.xcuitest_utils import XCUITestBuilder, XcodeBuildCommandNotFoundError


class XCUITestUploadToolTests(unittest.TestCase):

    def setUp(self):
        self.helper = XCUITestBuilder(
            xcodeproj_path=os.path.join(os.path.dirname(__file__), 'TestApp/TestApp.xcodeproj')
        )
        self.xctest_output_path = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.xctest_output_path)

    def test_load_xcuitest_products(self):
        self.helper._load_xcuitest_products()
        assert [('TestAppUITests', 'TestAppUITests.xctest', 'TestApp')] == self.helper.xcuitest_products

    def test_load_schemes_project_has_no_schemes(self):
        self.helper._load_schemes()
        assert [] == self.helper.scheme_list

    def test_copy_xctest(self):
        self.helper.derived_data_path = os.path.join(os.path.dirname(__file__), 'TestDerivedData')
        self.helper._copy_xctest('TestAppUITests', 'TestAppUITests.xctest', self.xctest_output_path)
        assert os.path.exists(os.path.join(self.xctest_output_path, 'TestAppUITests.xctest.zip'))

    def test_extract_app_metadata(self):
        self.helper.derived_data_path = os.path.join(os.path.dirname(__file__), 'TestDerivedData')
        self.helper.xcuitest_products = [('TestAppUITests', 'TestAppUITests.xctest', 'TestApp')]
        assert 'com.datatheorem.TestApp', '1' == self.helper._extract_app_metadata('TestAppUITests')

    def test_get_default_product_name(self):
        assert 'TestAppUITests' == self.helper._get_default_product_name()

    def test_get_default_scheme(self):
        assert 'TestAppUITests' == self.helper._get_default_scheme('TestAppUITests')

    def test_build_and_cleanup(self):
        try:
            self.helper._build_for_testing('TestAppUITests')
        except XcodeBuildCommandNotFoundError:
            # If `xcodebuild` isn't installed, skip this test (could be running in a CI pipeline)
            return

        built_app_path = os.path.join(self.helper.derived_data_path, 'Build', 'Products', 'Debug-iphoneos',
                                      'TestApp.app')
        assert os.path.exists(built_app_path)
        self.helper._cleanup()
        assert not os.path.exists(built_app_path)


if __name__ == "__main__":
    unittest.main()
