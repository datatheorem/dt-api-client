//
//  ViewController.swift
//  TestApp
//
//  Created by Eric Castro on 30/01/2019.
//  Copyright © 2019 Eric Castro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var testButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func testButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Congratulations!", message: "You passed this test", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "I said OK", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}

