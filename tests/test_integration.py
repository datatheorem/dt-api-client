from __future__ import absolute_import, unicode_literals

import os
import unittest
from datetime import datetime
try:
    from datetime import UTC  # Added in version 3.11
except ImportError:
    from datetime import timezone
    UTC = timezone.utc

from datatheorem_api_client import AuthenticationError, ResultsAPIClient

API_KEY = os.environ.get("RESULTS_API_KEY")
if not API_KEY and os.environ.get("CI"):
    raise EnvironmentError("Running on CI but no RESULTS_API_KEY environment variable")


@unittest.skipIf(not API_KEY, "No RESULTS_API_KEY set")
class ResultsAPIv2ClientIntegrationTests(unittest.TestCase):
    """Test suite that is ran against the real API with the SideScreen API key.
    """

    def setUp(self):
        self.api_client = ResultsAPIClient(API_KEY)

    def test_bad_api_key(self):
        # Given an API client with a bad API key
        api_client = ResultsAPIClient("wrong")

        # When trying to call the API, it returns the right error
        with self.assertRaises(AuthenticationError):
            api_client.list_mobile_apps()

    def test_mobile_apps(self):
        # Given an API client
        # When fetching the list of all mobile apps, it succeeds
        mobile_apps, next_cursor = self.api_client.list_mobile_apps()
        self.assertGreater(len(mobile_apps), 1)

        # When fetching a single mobile app
        mobile_app_id = mobile_apps[1].id
        # It succeeds
        mobile_app = self.api_client.get_mobile_app(mobile_app_id)
        self.assertTrue(mobile_app)

    def test_mobile_apps_multiple_subscription(self):
        # Given an API client
        # When fetching the list of all mobile apps, it succeeds
        mobile_apps, next_cursor = self.api_client.list_mobile_apps(subscription=["DYNAMIC", "STATIC"])
        self.assertGreater(len(mobile_apps), 1)

        # When fetching a single mobile app
        mobile_app_id = mobile_apps[1].id
        # It succeeds
        mobile_app = self.api_client.get_mobile_app(mobile_app_id)
        self.assertTrue(mobile_app)

    def test_mobile_apps_with_param(self):
        # Given an API client
        # When fetching the list of all ANDROID mobile apps, it succeeds
        mobile_apps, next_cursor = self.api_client.list_mobile_apps(platform="ANDROID")
        self.assertGreater(len(mobile_apps), 1)

    def test_mobile_apps_with_no_results(self):
        # Given an API client
        # When fetching an empty list of mobile apps, it succeeds
        mobile_apps, next_cursor = self.api_client.list_mobile_apps(results_since=datetime.now(UTC))
        self.assertEqual([], mobile_apps)

    def test_sdk_mobile_apps(self):
        # Given an API client
        # When fetching the list of all sdks, it succeeds
        sdks, next_cursor = self.api_client.list_sdks()
        # When fetching a single sdk
        sdk_id = sdks[0].id
        # Then we can list the mobile apps for this SDK
        mobile_apps, next_cursor = self.api_client.list_sdk_mobile_apps(sdk_id)
        # And it succeeds
        self.assertGreater(len(mobile_apps), 0)

    def test_sdks(self):
        # Given an API client
        # When fetching the list of all sdks, it succeeds
        sdks, next_cursor = self.api_client.list_sdks()
        # It succeeds
        self.assertGreater(len(sdks), 0)

    def test_security_findings(self):
        # Given an API client
        # When fetching the list of all security findings, it succeeds
        findings, next_cursor = self.api_client.list_security_findings()
        self.assertGreater(len(findings), 1)

        # When fetching a single finding
        finding_id = findings[1].id
        # It succeeds
        finding = self.api_client.get_security_finding(finding_id)
        self.assertTrue(finding)

    def test_security_findings_with_no_results(self):
        # Given an API client
        # When fetching an empty list of security findings
        findings, next_cursor = self.api_client.list_security_findings(results_since=datetime.now(UTC))
        self.assertEqual([], findings)

    def test_security_finding_targets(self):
        # Given an API client
        # When fetching the list of all targets, it succeeds
        targets, next_cursor = self.api_client.list_security_finding_targets()
        self.assertGreater(len(targets), 1)

        # When fetching a single target
        target_id = targets[1].id
        # It succeeds
        target = self.api_client.get_security_finding_target(target_id)
        self.assertTrue(target)

        # When patching a single target
        target_id = targets[1].id
        # It succeeds
        target = self.api_client.patch_security_finding_target(target_id, target.external_id)
        self.assertTrue(target)

    def test_security_finding_targets_with_no_results(self):
        # Given an API client
        # When fetching an empty list of targets
        targets, next_cursor = self.api_client.list_security_finding_targets(results_since=datetime.now(UTC))
        self.assertEqual([], targets)

    def test_get_jira_integration_config(self):
        # Given an API client
        # When fetching the Jira config, it succeeds
        response = self.api_client.get_jira_integration_config()
        self.assertTrue(response)

    def test_list_jira_integration_configs(self):
        # Given an API client
        # When fetching a list of jira configs
        response = self.api_client.list_jira_integration_configs()

        # Then it succeeds
        self.assertTrue(response)

    def test_mobile_app_insights(self):
        # Given an API client
        # When fetching the list of mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights()
        self.assertGreater(len(mobile_app_insights), 1)

    def test_mobile_app_insights__include_ios_entitlements(self):
        # Given an API client
        # When fetching the list of mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights(include=["ios_entitlements"])
        self.assertGreater(len(mobile_app_insights), 1)
        # And fetching an app's entitlements succeeds
        mobile_app_insights_mapping = {insight.mobile_app_bundle_id: insight for insight in mobile_app_insights}
        self.assertEqual(
            mobile_app_insights_mapping["com.saf.aranetCube"].ios_entitlements,
            '{"com.apple.developer.team-identifier": "PJ328K2NQ9", "application-identifier": "PJ328K2NQ9.com.saf.aranetCube"}'
        )

    def test_mobile_app_insights__include_ios_privacy_manifests(self):
        # Given an API client
        # When fetching the list of mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights(include=["ios_privacy_manifests"])
        self.assertGreater(len(mobile_app_insights), 1)
        # And fetching an app's privacy manifest succeeds
        mobile_app_insights_mapping = {insight.mobile_app_bundle_id: insight for insight in mobile_app_insights}
        self.assertGreater(len(mobile_app_insights_mapping["com.saf.aranetCube"].ios_privacy_manifests), 0)

    def test_mobile_app_insights__include_ios_entitlements_ios_privacy_manifests(self):
        # Given an API client
        # When fetching the list of mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights(
            include=["ios_entitlements", "ios_privacy_manifests"]
        )
        # And fetching an app's entitlements succeeds
        mobile_app_insights_mapping = {insight.mobile_app_bundle_id: insight for insight in mobile_app_insights}
        self.assertEqual(
            mobile_app_insights_mapping["com.saf.aranetCube"].ios_entitlements,
            '{"com.apple.developer.team-identifier": "PJ328K2NQ9", "application-identifier": "PJ328K2NQ9.com.saf.aranetCube"}'
        )
        # And fetching an app's privacy manifest succeeds
        self.assertGreater(len(mobile_app_insights_mapping["com.saf.aranetCube"].ios_privacy_manifests), 0)

    def test_list_sdks_in_mobile_apps(self):
        # Given an API client
        # When fetching the list of mobile app third-party SDKs, it succeeds
        mobile_app_sdks, next_cursor = self.api_client.list_sdks_in_mobile_apps()
        self.assertGreater(len(mobile_app_sdks), 1)

    def test_list_mobile_app_sdks(self):
        # Given an API client and a mobile app
        mobile_app_id = 5774415484813312  # IOS com.trianglegames.squarebird APP_STORE
        # When fetching the list of mobile app third-party SDKs, it succeeds
        mobile_app_sdks, next_cursor = self.api_client.list_mobile_app_sdks(mobile_app_id)
        self.assertGreater(len(mobile_app_sdks), 1)

    def test_get_mobile_app_sdk(self):
        # Given an API client and a mobile app and an SDK
        mobile_app_id = 5774415484813312  # IOS com.trianglegames.squarebird APP_STORE
        sdk_id = "01e753ff-cc83-53a6-baab-03873638ce1f"  # Adjust SDK
        # When fetching the list of mobile app third-party SDKs, it succeeds
        mobile_app_sdk = self.api_client.get_mobile_app_sdk(mobile_app_id, sdk_id)
        self.assertEqual(mobile_app_sdk.sdk_id, sdk_id)

    #####################
    # Tests to check that enums aren't breaking api calls with `list_*` methods
    #####################
    def test_list_mobile_apps_with_platform_and_release_type(self):
        # Given an API client
        # When fetching a list of mobile apps with some params
        # Then we see it succeeds
        self.api_client.list_mobile_apps(
            platform="ANDROID", release_type="APP_STORE",
        )

    def test_list_mobile_apps_with_scan_status(self):
        # Given an API client
        # When fetching a list of mobile apps with some params
        # Then we see it succeeds
        self.api_client.list_mobile_apps(scan_status="NO_SCANS_INITIATED",)

    def test_list_mobile_apps_with_subscription(self):
        # Given an API client
        # When fetching a list of mobile apps with some params
        # Then we see it succeeds
        self.api_client.list_mobile_apps(subscription="DYNAMIC")

    def test_list_security_findings(self):
        # Given an API client
        # When fetching a list of security findings with params
        # Then we see it succeeds
        self.api_client.list_security_findings(status_group="CLOSED")

    def test_list_security_finding_targets(self):
        # Given an API client
        # When fetching a list of security finding targets with params
        # Then we see it succeeds
        self.api_client.list_security_finding_targets(status_group="OPEN")

    def test_mobile_app_insights_with_param(self):
        # Given an API client
        # When fetching the list of all ANDROID mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights(mobile_app_platform="ANDROID")
        self.assertGreater(len(mobile_app_insights), 1)

    def test_mobile_app_insights_with_no_results(self):
        # Given an API client
        # When fetching an empty list of mobile app insights, it succeeds
        mobile_app_insights, next_cursor = self.api_client.list_mobile_app_insights(
            mobile_app_platform="ANDROID",
            mobile_app_release_type="PRE_PROD",
            mobile_app_bundle_id="com.non.existent.app",
        )
        self.assertEqual([], mobile_app_insights)
