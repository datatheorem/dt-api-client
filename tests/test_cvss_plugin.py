from __future__ import absolute_import, unicode_literals

import unittest

from datatheorem_api_client import SecurityFinding

from cvss_plugin import CvssPlugin


class CvssPluginTests(unittest.TestCase):
    def test(self):
        # Given a finding
        finding = SecurityFinding(
            id="1",
            date_created="2016-11-02T19:25:16.664990-00:00",
            title="Test",
            severity="HIGH",
            category="DATA_LOSS_TO_HACKERS",
            exploitability="EASY",
            description="test",
            recommendation="test",
            mobile_app_id="123",
            importance_tags=[],
            links=[],
            portal_url="test",
            aggregated_status="OPEN",
            cvss_score=0.0,
            cvss_vector="",
            is_permanently_closed=False,
            issue_type_id="12345678-1234-5678-1234-567812345678",
        )

        # When generating a CVSS score, it succeeds
        score = CvssPlugin.compute_cvss_score(finding)
        self.assertEqual(7.0, score)
