# coding: utf-8
from __future__ import absolute_import, unicode_literals

from datatheorem_api_client.results_api.security_finding import SecurityFinding


class CvssPlugin:
    """Compute the CVSS score for a security issue returned by the Data Theorem Results API.
    """

    BASE_SCORE = {
        "DATA_AT_REST_EXPOSURE": 1,
        "DATA_IN_TRANSIT_EXPOSURE": 3,
        "DATA_LOSS_TO_HACKERS": 3,
        "DATA_EXPOSURE_TO_THIRD_PARTY_APPS": 2,
        "UNAUTHORIZED_DATA_COLLECTION": 1.5,
    }

    SEVERITY_MODIFIER = {"HIGH": 2, "MEDIUM": 1, "LOW": 0}

    EXPLOITABILITY_MODIFIER = {"EASY": 2, "MODERATE": 1, "DIFFICULT": 0}

    SECURITY_P1_MODIFIER = 2

    @classmethod
    def compute_cvss_score(cls, security_finding):
        # type: (SecurityFinding) -> float
        score = cls.BASE_SCORE[security_finding.category]
        score += cls.SEVERITY_MODIFIER[security_finding.severity]
        score += cls.EXPLOITABILITY_MODIFIER[security_finding.exploitability]
        if "SECURITY_P1" in security_finding.importance_tags:
            score += cls.SECURITY_P1_MODIFIER
        return float(score)
