from io import FileIO
from typing import Optional

import requests


class UploadApiClient:
    """Api client for accessing Data Theorem's Upload API, for uploading pre-production builds."""

    UPLOAD_INIT_URL = "https://api.securetheorem.com/uploadapi/v1/upload_init"
    UPLOAD_XCUITEST_ATTACHMENT_INIT_URL = "https://api.securetheorem.com/uploadapi/v1/upload_xcuitest_attachment_init"

    def __init__(self, api_key: str) -> None:
        self._api_key = api_key

    def upload(
        self,
        file_descriptor: FileIO,
        sourcemap: Optional[FileIO] = None,
        comments: Optional[str] = None,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ) -> None:
        """Uploads the file descriptor as a Pre Prod App via the Data Theorem API.

        Args:
            file_descriptor (file object): The App package to upload (opened file object).
            sourcemap (file object): An optional sourcemap file for de-obfuscating Android scan results.
            comments (str, optional): Comments.
            username (str, optional): Username for dynamic scanning.
            password (str, optional): Password for dynamic scanning.

        Raises:
            requests.exceptions.HTTPError: An exception will be raised if the upload fail.

        Returns:
            The JSON response as a dict.
        """
        # Initial request to retrieve an upload URL
        initial_request = requests.post(
            self.UPLOAD_INIT_URL,
            headers={"Authorization": "Bearer {0}".format(self._api_key)},
        )
        initial_request.raise_for_status()

        # Actually upload the App build and the optional meta data
        meta = {"comments": comments, "username": username, "password": password}

        upload_url = initial_request.json().get("upload_url")
        files = dict(file=file_descriptor)
        if sourcemap:
            files["sourcemap"] = sourcemap
        upload_request = requests.post(
            upload_url,
            data=meta,  # type: ignore
            files=files,
        )
        upload_request.raise_for_status()

    def upload_xcuitest_attachment(
        self, file_descriptor: FileIO, bundle_id: str, version: str, release_type: str
    ) -> None:
        """Uploads the file descriptor as a XCUITest attachment via the Data Theorem API.

        Args:
            file_descriptor (file object): The App package to upload (opened file object).

        Raises:
            requests.exceptions.HTTPError: An exception will be raised if the upload fail.

        Returns:
            The JSON response as a dict.
        """
        # Initial request to retrieve an upload URL
        initial_request = requests.post(
            self.UPLOAD_XCUITEST_ATTACHMENT_INIT_URL,
            headers={"Authorization": "Bearer {0}".format(self._api_key)},
        )
        initial_request.raise_for_status()

        # Actually upload the App build and the optional meta data
        meta = {
            "bundle_id": bundle_id,
            "version": version,
            "release_type": release_type,
        }

        upload_url = initial_request.json().get("upload_url")
        upload_request = requests.post(upload_url, data=meta, files=dict(file=file_descriptor))

        upload_request.raise_for_status()
