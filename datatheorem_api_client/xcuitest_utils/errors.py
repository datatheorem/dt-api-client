class MultipleXCUITestError(Exception):
    """
    Raised when a project has more than one UI test product and thus
    cannot be chosen automatically (The user needs to specify it)
    """

    pass


class MultipleSchemesError(Exception):
    """
    Raised when a project has more than one scheme and thus cannot
    be chosen automatically (The user needs to specify it)
    """

    pass


class MustBuildFirstError(Exception):
    """
    Raised when extracting an xctest bundle without having built it first
    """

    pass


class NoXCTestFoundError(Exception):
    """
    Raised when xctest don't exist after building and trying to extract them
    """

    pass


class NoProductsFoundError(Exception):
    """
    Raised when no products are found in a xcodeproj (should never be fired)
    """

    pass


class XCodeBuildFailedError(Exception):
    """
    Raised when `xcodebuild` fails to build the specified Xcode project
    """

    pass


class InvalidXcodeProjError(Exception):
    """
    Raised when a non existent or invalid xcode project path is given
    """

    pass


class NoTestTargetsFoundError(Exception):
    """
    Raised when the test target (the main binary's target) associated to the XCUITest cannot be found
    """

    pass


class CantExtractAppMetadataError(Exception):
    """
    Raised when the Info plist cannot be found for the test target to which the UI test is associated to
    """

    pass


class XcodeBuildCommandNotFoundError(Exception):
    """
    Raised when xcodebuild command line tool is not installed in the system
    """

    pass
