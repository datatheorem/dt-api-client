import glob
import os
import plistlib
import re
import shutil
import subprocess
import tempfile
from distutils.spawn import find_executable
from typing import List, Optional, Tuple

from datatheorem_api_client.xcuitest_utils.errors import (
    CantExtractAppMetadataError,
    InvalidXcodeProjError,
    MultipleSchemesError,
    MultipleXCUITestError,
    MustBuildFirstError,
    NoProductsFoundError,
    NoTestTargetsFoundError,
    NoXCTestFoundError,
    XcodeBuildCommandNotFoundError,
    XCodeBuildFailedError,
)


class XCUITestBuilder(Exception):
    def __init__(self, xcodeproj_path: str, xcworkspace_path: Optional[str] = None) -> None:
        """
        Initializes instance based on provided xcode project bundle
        """
        if not os.path.exists(xcodeproj_path):
            raise InvalidXcodeProjError(xcodeproj_path)

        self.xcodeproj_path = xcodeproj_path

        # tuple sequence: product name, xctest name - most of the times will be  the same (appending .xctest)
        # but if user manually changed one of the two, name can no longer be assumed
        self.xcuitest_products: List[Tuple[str, str, str]] = []
        self.scheme_list: List[str] = []
        self.derived_data_path: Optional[str] = None
        self.xcworkspace_path = xcworkspace_path

        self._load_xcuitest_products()

    @staticmethod
    def archive_xctest(xctest_path: str, output_path: str) -> str:
        """
        Puts the xctest bundle contents into a zip file so that it can be uploaded
        :param xctest_path: the path to the xctest bundle
        :param output_path: the path to the resulting zip archive
        :return the final zip path
        """
        container_path = tempfile.mkdtemp()
        root_path = os.path.join(container_path, os.path.basename(xctest_path))
        shutil.copytree(xctest_path, root_path)
        archive_path = shutil.make_archive(output_path, "zip", container_path)
        shutil.rmtree(container_path)
        return archive_path

    @classmethod
    def create_from_xcworkspace(cls, xcodeproj_path: str, xcworkspace_path: str) -> "XCUITestBuilder":
        # Allow to build a product using a workspace instead of a project
        with open(xcworkspace_path):
            # Raises an exception if the path is invalid
            pass

        return cls(xcodeproj_path, xcworkspace_path)

    def build_xctest(
        self,
        product_name: Optional[str] = None,
        scheme_name: Optional[str] = None,
        xctest_output_dir: Optional[str] = None,
    ) -> Tuple[str, str, str]:
        """
        Prepares the environment and builds the Xcode project in "build-for-testing" mode
        so that the UI Testing products are also built in their separate "Runner" app
        and thus extracted (zipped) to an output folder
        :param product_name: the product to be built - must be specified if project has more than one product
        :param scheme_name: the scheme to be built - must be specified if project has more than one user-defined scheme
        :param xctest_output_dir: the path to which the built xctest will be copied to
        :returns output_path: the final
        """

        try:
            if product_name:  # If user specified a product_name, use that one
                selected_product_name = product_name
            else:  # If no product_name is specified, use default
                selected_product_name = self._get_default_product_name()  # raises exception if multiple products

            selected_xctest_name = self._get_xctest_from_product(selected_product_name)

            if scheme_name:  # If user specified a scheme_name, use that one
                selected_scheme_name = scheme_name
            else:  # If no scheme_name is specified, use default
                selected_scheme_name = self._get_default_scheme(product_name)  # raises exception if multiple schemes

            # Build for testing
            self._build_for_testing(selected_scheme_name)

            # Extract main apps metadata that can later be used so user doesn't have to specify it manually
            app_bundle_id, app_version = self._extract_app_metadata(selected_product_name)

            # Output destination defaults to script's directory if none given
            if not xctest_output_dir:
                xctest_output_dir = os.getcwd()

            # Copy the built xctest file
            final_output_path = self._copy_xctest(selected_product_name, selected_xctest_name, xctest_output_dir)

        finally:
            # Remove all built products that are no longer needed
            self._cleanup()

        # Return the path to the zipped xctest bundle, the apps bundle ID and version extracted from the Info.plist
        return final_output_path, app_bundle_id, app_version

    def _load_xcuitest_products(self) -> None:
        """
        Loads the list of product names and their respective xctest filenames
        """
        pbxproj_path = os.path.join(self.xcodeproj_path, "project.pbxproj")
        with open(pbxproj_path, "rb") as pbxproj_file:
            pbxproj_contents = pbxproj_file.read().decode("utf-8")

            # .pbxproj is neither a plist nor JSON file but an old NeXT file type format
            # regex is the most suitable parsing tool for this particular case

            # We look UI test products in the Xcode project file
            # These are identified with productType = com.apple.product-type.bundle.ui-testing

            matches = re.findall(
                r'name = "*.*"*;\s*productName = (.*)\s*;\s*'
                r"productReference = .* /\* (.*) \*/;\s*"
                r'productType = "com\.apple\.product-type\.bundle\.ui-testing";',
                pbxproj_contents,
                re.MULTILINE,
            )
            # Put the product name and xctest filename (which can differ) in the resulting list

            result = []
            for match in matches:
                product_name, xctest_name = match
                product_name = product_name.strip('"')
                xctest_name = xctest_name.strip('"')
                build_configuration_id_matches = re.findall(
                    r"Begin XCConfigurationList section.*?"
                    + r"Build configuration list for PBXNativeTarget \""
                    + product_name
                    + '"'
                    r".*?buildConfigurations = \((.*?);",
                    pbxproj_contents,
                    re.MULTILINE | re.DOTALL,
                )

                if len(build_configuration_id_matches) == 0:
                    raise NoTestTargetsFoundError()

                # We need to remove the possible tabulation and the comment from the result
                build_configuration_id = build_configuration_id_matches[0].strip().split(" ")[0]

                test_target_matches = re.findall(
                    build_configuration_id + r".*?TEST_TARGET_NAME = (.*?);.*?};",
                    pbxproj_contents,
                    re.MULTILINE | re.DOTALL,
                )

                if len(test_target_matches) == 0:
                    raise NoTestTargetsFoundError()

                test_target_name = test_target_matches[0]
                result.append((product_name, xctest_name, test_target_name))

            if len(result) == 0:
                raise NoProductsFoundError()

        self.xcuitest_products = result

    def _extract_app_metadata(self, product_name: str) -> Tuple[str, str]:
        """
        Gets bundle id and version info from the main binary app, that it can later be used to
        associate the extracted UI Testing bundle with it's corresponding app when uploaded
        :param product_name: The UI Testing bundle's product name
        :return: a tuple including the app's bundle id and its version number extracted from the Info.plist
        """

        # Check if app was built first
        if not self.derived_data_path:
            raise MustBuildFirstError()

        # Get the test target for the specified product_name
        test_target_name = [x[2] for x in self.xcuitest_products if x[0] == product_name][0]

        # Find the xctest bundle according to Xcode's path convention
        info_plist_path_wildcard = os.path.join(
            self.derived_data_path,
            "Build",
            "Products",
            "*",
            "{}.app".format(test_target_name),
            "Info.plist",
        )
        info_plist_from_paths = glob.glob(info_plist_path_wildcard)
        if len(info_plist_from_paths) == 0:
            raise CantExtractAppMetadataError()

        with open(info_plist_from_paths[0], "rb") as fp:
            app_info = plistlib.load(fp)
        return app_info["CFBundleIdentifier"], app_info["CFBundleVersion"]

    def _load_schemes(self) -> None:
        """
        Loads the list of schemes to be used for building the xctest files later
        """
        xcscheme_files = []  # type: List[str]
        xcscheme_files += glob.glob(os.path.join(self.xcodeproj_path, "xcshareddata", "xcschemes", "*.xcscheme"))
        xcscheme_files += glob.glob(
            os.path.join(
                self.xcodeproj_path,
                "xcuserdata",
                "*.xcuserdatad",
                "xcschemes",
                "*.xcscheme",
            )
        )

        scheme_list = []
        for xcscheme_file in xcscheme_files:
            scheme_name, _ = os.path.splitext(xcscheme_file)
            scheme_list.append(os.path.basename(scheme_name))

        self.scheme_list = scheme_list

    def _get_default_product_name(self) -> str:
        """
        Gets the product name for the xcodeproject.
        If there is more than one product containing UI tests, an exception is thrown
        """
        if len(self.xcuitest_products) > 1:
            raise MultipleXCUITestError()

        return self.xcuitest_products[0][0]

    def _get_default_scheme(self, product_name: Optional[str] = None) -> str:
        """
        Gets the default scheme for a given product name in the xcodeproject.
        If there is more than one scheme, an exception is thrown
        """
        if not product_name:
            product_name = self._get_default_product_name()

        if len(self.scheme_list) == 0:
            self._load_schemes()

        # If scheme_list has more than 1 element, means there are more UI test schemes
        # And thus user must specify it
        if len(self.scheme_list) > 1:
            raise MultipleSchemesError()

        # If scheme_list is empty, means we must use product name as scheme name as per Xcode default behavior
        elif len(self.scheme_list) == 0:
            selected_scheme_name = product_name

        # If only one scheme exists for building the xctest file, then choose it automatically (most cases?)
        else:
            selected_scheme_name = self.scheme_list[0]

        return selected_scheme_name

    def _build_for_testing(self, scheme_name: str) -> None:
        """
        Builds the project in build-for-testing mode, so that the UI tests will also be built
        and packaged in their '...-Runner.app'
        """

        # Check if `xcodebuild` is installed in the system
        if find_executable("xcodebuild") is None:
            raise XcodeBuildCommandNotFoundError()

        self.derived_data_path = tempfile.mkdtemp()
        xcodebuild_param_list = [
            "xcodebuild",
            "build-for-testing",
            "-allowProvisioningUpdates",
            "-sdk",
            "iphoneos",
            "-scheme",
            scheme_name,
            "-derivedDataPath",
            self.derived_data_path,
        ]
        if self.xcworkspace_path:
            xcodebuild_param_list.extend(["-workspace", self.xcworkspace_path])
        else:
            xcodebuild_param_list.extend(["-project", self.xcodeproj_path])

        try:
            subprocess.check_output(xcodebuild_param_list)
        except subprocess.CalledProcessError:
            raise XCodeBuildFailedError()

    def _cleanup(self) -> None:
        """
        Cleans up Xcode's built products
        """
        if self.derived_data_path:
            shutil.rmtree(self.derived_data_path)
            self.derived_data_path = None

    def _copy_xctest(self, product_name: str, xctest_name: str, xctest_output_dir: str) -> str:
        """
        Finds and copies the xctest from a project that has been "built for testing", meaning that it should
        include the UI Testing product in a xctest bundle inside a "<product name>-Runner.app" bundle
        :param product_name: the product name associated with the xctest to be extracted
        :param xctest_name: the xctest filename to be extracted
        :param xctest_output_dir: the directory to copy the xctest to (default is current directory)
        """

        # Verify the output path for built products is set
        if not self.derived_data_path:
            raise MustBuildFirstError()

        # Find the xctest bundle according to Xcode's path convention
        xctest_path_wildcard = os.path.join(
            self.derived_data_path,
            "Build",
            "Products",
            "*",
            "{}-Runner.app".format(product_name),
            "PlugIns",
            xctest_name,
        )
        xctest_from_paths = glob.glob(xctest_path_wildcard)

        # An unknown error may have caused the xctest to not be built
        if len(xctest_from_paths) == 0:
            raise NoXCTestFoundError()

        # The user may have specified an output path to the xctest instead of a directory
        output_path = xctest_output_dir
        if os.path.isdir(xctest_output_dir):
            output_path = os.path.join(xctest_output_dir, xctest_name)

        if os.path.exists(output_path):
            shutil.rmtree(output_path)

        # Zip the resulting xctest bundle into the desired output path
        final_path = XCUITestBuilder.archive_xctest(xctest_from_paths[0], output_path)

        return final_path

    def _get_xctest_from_product(self, selected_product_name: str) -> str:
        for product_name, xctest_name, _ in self.xcuitest_products:
            if product_name == selected_product_name:
                return xctest_name

        raise NoXCTestFoundError()
