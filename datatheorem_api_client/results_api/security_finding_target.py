from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.links import ApiResourceLink
from datatheorem_api_client.results_api.types import StatusType


@dataclass(frozen=True)
class SecurityFindingTargetStatus:
    status: StatusType
    date: str
    build_version: Optional[str]

    @classmethod
    def from_dict(cls, status_dict: Dict[str, Any]) -> "SecurityFindingTargetStatus":
        return cls(
            status=status_dict["status"],
            date=status_dict["date"],
            build_version=status_dict.get("build_version", None),
        )


@dataclass(frozen=True)
class SecurityFindingTarget:
    """A target found when scanning an app.

    Targets are called "affected components" in the Data Theorem portal.
    A description of each field is available at https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    id: int
    date_created: str
    text: str
    current_status: StatusType
    current_status_date: str
    mobile_app_id: int
    security_finding_id: str
    portal_url: str
    statuses: List[SecurityFindingTargetStatus]
    links: List[ApiResourceLink]
    results_last_updated: Optional[str] = None
    external_id: Optional[str] = None
    external_bug_tracker_url: Optional[str] = None

    @classmethod
    def from_dict(cls, target_dict: Dict[str, Any]) -> "SecurityFindingTarget":
        return cls(
            id=int(target_dict["id"]),
            date_created=target_dict["date_created"],
            text=target_dict["text"],
            current_status=target_dict["current_status"],
            current_status_date=target_dict["current_status_date"],
            mobile_app_id=int(target_dict["mobile_app_id"]),
            security_finding_id=target_dict["security_finding_id"],
            portal_url=target_dict["portal_url"],
            statuses=[SecurityFindingTargetStatus.from_dict(status) for status in target_dict.get("statuses", [])],
            links=[ApiResourceLink.from_dict(link) for link in target_dict.get("links", [])],
            results_last_updated=target_dict.get("results_last_updated", None),
            external_id=target_dict.get("external_id", None),
            external_bug_tracker_url=target_dict.get("external_bug_tracker_url", None),
        )
