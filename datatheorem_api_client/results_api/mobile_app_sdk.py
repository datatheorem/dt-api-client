from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.links import ApiResourceLink
from datatheorem_api_client.results_api.types import CategoryType


@dataclass(frozen=True)
class MobileAppSdkHistoryEvent:
    present: bool
    date: str

    @classmethod
    def from_dict(cls, event_dict: Dict[str, Any]) -> "MobileAppSdkHistoryEvent":
        return cls(
            present=event_dict["present"],
            date=event_dict["date"],
        )


@dataclass(frozen=True)
class SDKLicenseInformationResponse:
    license: str
    detail: str

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SDKLicenseInformationResponse":
        return cls(
            license=_dict["license"],
            detail=_dict["detail"],
        )


@dataclass(frozen=True)
class SDKDataTypeResponse:
    data_type: str
    store_data_type: str
    data_type_icon_url: str

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SDKDataTypeResponse":
        return cls(
            data_type=_dict["data_type"],
            store_data_type=_dict["store_data_type"],
            data_type_icon_url=_dict["data_type_icon_url"],
        )


@dataclass(frozen=True)
class SDKUnreportedDataTypeAffectedComponentResponse:
    code_location: str
    rel: str
    security_finding_id: str
    security_finding_target_id: str

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SDKUnreportedDataTypeAffectedComponentResponse":
        return cls(
            code_location=_dict["code_location"],
            rel=_dict["rel"],
            security_finding_id=_dict["security_finding_id"],
            security_finding_target_id=_dict["security_finding_target_id"],
        )


@dataclass(frozen=True)
class SDKUnreportedDataTypeResponse(SDKDataTypeResponse):
    affected_components: List[SDKUnreportedDataTypeAffectedComponentResponse]

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SDKUnreportedDataTypeResponse":
        return cls(
            data_type=_dict["data_type"],
            store_data_type=_dict["store_data_type"],
            data_type_icon_url=_dict["data_type_icon_url"],
            affected_components=[
                SDKUnreportedDataTypeAffectedComponentResponse.from_dict(affected_component)
                for affected_component in _dict.get("affected_components", [])
            ],
        )


@dataclass(frozen=True)
class SDKPrivacyManifestResponse:
    is_manifest_required_by_apple: bool
    is_manifest_included: bool
    is_manifest_available: bool
    sdk_manifest_data_types: List[SDKDataTypeResponse]
    unreported_data_types: List[SDKUnreportedDataTypeResponse]
    has_digital_signature: Optional[str] = None
    sdk_manifest_as_json: Optional[str] = None

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SDKPrivacyManifestResponse":
        return cls(
            is_manifest_required_by_apple=_dict["is_manifest_required_by_apple"],
            is_manifest_included=_dict["is_manifest_included"],
            is_manifest_available=_dict["is_manifest_available"],
            has_digital_signature=_dict["has_digital_signature"],
            sdk_manifest_as_json=_dict["sdk_manifest_as_json"],
            sdk_manifest_data_types=[
                SDKDataTypeResponse.from_dict(data_type) for data_type in _dict["sdk_manifest_data_types"]
            ],
            unreported_data_types=[
                SDKUnreportedDataTypeResponse.from_dict(data_type) for data_type in _dict["unreported_data_types"]
            ],
        )


@dataclass(frozen=True)
class MobileAppSdk:
    """Third-party SDKs related to an app registered in your organization's Data Theorem account.

    A description of each field is available at
    https://docs.securetheorem.com/mobile_security_results/mobile_app_sdks.html
    """

    id: str
    sdk_id: str
    mobile_app_id: int
    date_created: str
    currently_present: bool
    history: List[MobileAppSdkHistoryEvent]
    title: str
    description: str
    category: CategoryType
    url: str
    portal_url: str
    links: List[ApiResourceLink]
    has_ios_privacy_manifest: bool
    associated_security_finding_target_ids: Optional[List[int]] = None
    associated_domains: Optional[List[str]] = None
    license_information: Optional[List[SDKLicenseInformationResponse]] = None
    ios_privacy_manifest: Optional[SDKPrivacyManifestResponse] = None

    @classmethod
    def from_dict(cls, sdk_dict: Dict[str, Any]) -> "MobileAppSdk":
        try:
            ios_privacy_manifests_dict = sdk_dict["ios_privacy_manifests"]

        except KeyError:
            ios_privacy_manifest = None

        else:
            ios_privacy_manifest = SDKPrivacyManifestResponse.from_dict(ios_privacy_manifests_dict)

        return cls(
            id=sdk_dict["id"],
            sdk_id=sdk_dict["sdk_id"],
            mobile_app_id=int(sdk_dict["mobile_app_id"]),
            date_created=sdk_dict["date_created"],
            currently_present=sdk_dict["currently_present"],
            history=[MobileAppSdkHistoryEvent.from_dict(link) for link in sdk_dict.get("history", [])],
            title=sdk_dict["title"],
            description=sdk_dict["description"],
            category=sdk_dict["category"],
            url=sdk_dict["url"],
            portal_url=sdk_dict["portal_url"],
            links=[ApiResourceLink.from_dict(link) for link in sdk_dict.get("links", [])],
            has_ios_privacy_manifest=sdk_dict["has_ios_privacy_manifest"],
            associated_security_finding_target_ids=sdk_dict.get("associated_security_finding_target_ids"),
            associated_domains=sdk_dict.get("associated_domains"),
            license_information=[
                SDKLicenseInformationResponse.from_dict(license_info)
                for license_info in sdk_dict.get("license_information", [])
            ],
            ios_privacy_manifest=ios_privacy_manifest,
        )
