from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.types import JiraDynamicValueType, JiraExportType, JiraFieldType


@dataclass(frozen=True)
class SeverityFieldConfig:
    field_id: str
    high_severity_value_id: str
    medium_severity_value_id: str
    low_severity_value_id: str

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "SeverityFieldConfig":
        field_id = _dict["field_id"]
        high_severity_value_id = _dict["high_severity_value_id"]
        medium_severity_value_id = _dict["medium_severity_value_id"]
        low_severity_value_id = _dict["low_severity_value_id"]

        return cls(
            field_id=field_id,
            high_severity_value_id=high_severity_value_id,
            medium_severity_value_id=medium_severity_value_id,
            low_severity_value_id=low_severity_value_id,
        )


@dataclass(frozen=True)
class JiraField:
    field_id: str
    type: JiraFieldType

    option_value: Optional[str]
    number_value: Optional[int]
    string_value: Optional[str]
    user_value: Optional[str]
    string_array_value: List[str] = field(default_factory=list)
    string_multiselect_value: List[str] = field(default_factory=list)
    component_array_value: List[str] = field(default_factory=list)
    option_array_value: List[str] = field(default_factory=list)

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "JiraField":
        field_id = _dict["field_id"]
        _type = _dict["type"]

        return cls(
            field_id=field_id,
            type=_type,
            string_value=_dict.get("string_value"),
            string_array_value=_dict.get("string_array_value", []),
            string_multiselect_value=_dict.get("string_multiselect_value", []),
            component_array_value=_dict.get("component_array_value", []),
            option_array_value=_dict.get("option_array_value", []),
            option_value=_dict.get("option_value"),
            number_value=_dict.get("number_value"),
            user_value=_dict.get("user_value"),
        )

    @property
    def value(self) -> Any:
        """Shortcut to mapping what field this Jira Field holds (eg STRING type should have `self.string_value`, etc)"""
        type_to_value = {
            "STRING": self.string_value,
            "STRING_ARRAY": self.string_array_value,
            "STRING_MULTISELECT": self.string_multiselect_value,
            "COMPONENT_ARRAY": self.component_array_value,
            "OPTION_ARRAY": self.option_array_value,
            "OPTION": self.option_value,
            "NUMBER": self.number_value,
            "USER": self.user_value,
        }

        return type_to_value[self.type]


@dataclass(frozen=True)
class StaticField:
    field_id: str
    field_value: str

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "StaticField":
        field_id = _dict["field_id"]
        field_value = _dict["field_value"]

        return cls(field_id=field_id, field_value=field_value)


@dataclass(frozen=True)
class DynamicField:
    field_id: str
    value: JiraDynamicValueType

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "DynamicField":
        field_id = _dict["field_id"]
        value = _dict["value"]

        return cls(field_id=field_id, value=value)


@dataclass(frozen=True)
class JiraIntegrationConfig:
    """A Jira Configuration that describes how to communicate to a specific Jira instance

    A description of each field is available at https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    id: int
    base_url: str
    username: str
    password: str
    type_of_issue_name: str
    export_filter: JiraExportType
    export_pre_prod: bool
    export_prod: bool
    project_key_or_id: str
    is_global: bool
    raw_mobile_app_id: Optional[int] = None
    severity_field_config: Optional[SeverityFieldConfig] = None
    jira_fields: List[JiraField] = field(default_factory=list)
    static_fields: List[StaticField] = field(default_factory=list)
    dynamic_fields: List[DynamicField] = field(default_factory=list)

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "JiraIntegrationConfig":
        id = int(_dict["id"])
        base_url = _dict["base_url"]
        username = _dict["username"]
        password = _dict["password"]
        type_of_issue_name = _dict["type_of_issue_name"]
        export_filter = _dict["export_filter"]
        export_pre_prod = _dict["export_pre_prod"]
        export_prod = _dict["export_prod"]
        project_key_or_id = _dict["project_key_or_id"]
        is_global = _dict["is_global"]
        raw_mobile_app_id = _dict.get("raw_mobile_app_id", None)
        severity_field_config = (
            SeverityFieldConfig.from_dict(_dict["severity_field_config"])
            if _dict.get("severity_field_config")
            else None
        )
        jira_fields = [JiraField.from_dict(field) for field in _dict.get("jira_fields", [])]
        static_fields = [StaticField.from_dict(field) for field in _dict.get("static_fields", [])]
        dynamic_fields = [DynamicField.from_dict(field) for field in _dict.get("dynamic_fields", [])]

        return cls(
            id,
            base_url,
            username,
            password,
            type_of_issue_name,
            export_filter,
            export_pre_prod,
            export_prod,
            project_key_or_id,
            is_global,
            raw_mobile_app_id=raw_mobile_app_id,
            severity_field_config=severity_field_config,
            jira_fields=jira_fields,
            static_fields=static_fields,
            dynamic_fields=dynamic_fields,
        )
