from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, List, Optional


@dataclass(frozen=True)
class MobileFindingPreviewField:
    id: str
    title: str

    @classmethod
    def from_dict(cls, finding: Dict[str, Any]) -> "MobileFindingPreviewField":
        return cls(id=finding["id"], title=finding["title"])


@dataclass(frozen=True)
class SDKMobileApp:
    id: int

    bundle_id: str
    platform: str

    has_data_firewall_enabled: bool
    name: str
    version: str

    open_findings_count: int
    top_three_findings: List[MobileFindingPreviewField]

    icon_url: Optional[str] = None

    # The next two fields are mutually exclusive
    date_added: Optional[datetime] = None
    date_removed: Optional[datetime] = None

    @classmethod
    def from_dict(cls, mobile_app: Dict[str, Any]) -> "SDKMobileApp":
        return cls(
            id=mobile_app["id"],
            bundle_id=mobile_app["bundle_id"],
            platform=mobile_app["platform"],
            has_data_firewall_enabled=mobile_app["has_data_firewall_enabled"],
            name=mobile_app["name"],
            version=mobile_app["version"],
            open_findings_count=mobile_app["open_findings_count"],
            top_three_findings=[MobileFindingPreviewField.from_dict(x) for x in mobile_app["top_three_findings"]],
            icon_url=mobile_app.get("icon_url"),
            date_added=datetime.fromisoformat(mobile_app["date_added"]) if "date_added" in mobile_app else None,
            date_removed=datetime.fromisoformat(mobile_app["date_removed"]) if "date_removed" in mobile_app else None,
        )


@dataclass(frozen=True)
class SDK:
    """Third-party SDKs related to an app registered in your organization's Data Theorem account.

    A description of each field is available at
    https://docs.securetheorem.com/mobile_security_results/mobile_app_sdks.html
    """

    id: str
    category: str
    description: str
    title: str

    related_open_security_finding_beyond_subscription_level_count: int
    related_open_security_finding_count: int

    logo_url: Optional[str] = None
    mobile_app_ids_curently_with_this_sdk: Optional[List[int]] = None
    sdk_finding_ids: Optional[List[str]] = None
    url: Optional[str] = None

    license_information: Optional[List["SDKLicenseInformation"]] = None

    @classmethod
    def from_dict(cls, sdk_dict: Dict[str, Any]) -> "SDK":
        return cls(
            id=sdk_dict["id"],
            category=sdk_dict["category"],
            description=sdk_dict["description"],
            title=sdk_dict["title"],
            related_open_security_finding_beyond_subscription_level_count=(
                int(sdk_dict["related_open_security_finding_beyond_subscription_level_count"])
            ),
            related_open_security_finding_count=(int(sdk_dict["related_open_security_finding_count"])),
            logo_url=sdk_dict.get("logo_url"),
            mobile_app_ids_curently_with_this_sdk=(
                [int(id_) for id_ in sdk_dict["mobile_app_ids_curently_with_this_sdk"]]
                if "mobile_app_ids_curently_with_this_sdk" in sdk_dict
                else None
            ),
            sdk_finding_ids=sdk_dict.get("sdk_finding_ids"),
            url=sdk_dict.get("url"),
            license_information=(
                [SDKLicenseInformation.from_dict(x) for x in sdk_dict["license_information"]]
                if "license_information" in sdk_dict
                else None
            ),
        )


@dataclass(frozen=True)
class SDKLicenseInformation:
    license: str
    detail: str

    @classmethod
    def from_dict(cls, license_dict: Dict[str, Any]) -> "SDKLicenseInformation":
        return cls(
            license=license_dict["license"],
            detail=license_dict["detail"],
        )
