from typing_extensions import Literal

PlatformType = Literal["ANDROID", "ANDROID_AMAZON", "ANDROID_OCULUS", "WINDOWS_PHONE", "IOS"]

ReleaseType = Literal["APP_STORE", "PRE_PROD", "APP_STORE_THIRD_PARTY"]

BlockedScanReasonType = Literal["NOT_BLOCKED", "NO_CREDENTIALS", "CORRUPTED_APP_PACKAGE"]

ScanStatusType = Literal["NO_SCANS_INITIATED", "SCAN_ONGOING", "SCAN_COMPLETED"]

SubscriptionType = Literal["APP_LOGIC", "DYNAMIC", "NO_SUBSCRIPTION", "P1_ALERTS", "STATIC"]

StatusGroupType = Literal["OPEN", "CLOSED"]

SeverityType = Literal["HIGH", "MEDIUM", "LOW"]

ImportanceTagType = Literal[
    "SECURITY_P1",
    "SECURITY",
    "APPLE",
    "APPLE_P1",
    "GOOGLE",
    "GOOGLE_P1",
    "DEFENSE_IN_DEPTH",
    "SERVER_SIDE",
    "CUSTOMER_POLICY",
]

StatusType = Literal[
    "OPEN",
    "NEW",
    "CLOSED_FIXED",
    "CLOSED_RISK_ACCEPTED",
    "CLOSED_COMPENSATING_CONTROL",
    "CLOSED_ITEM_NOT_FOUND",
    "OPEN_READY_TO_RESCAN",
]

CategoryType = Literal[
    "DATA_AT_REST_EXPOSURE",
    "DATA_IN_TRANSIT_EXPOSURE",
    "DATA_LOSS_TO_HACKERS",
    "DATA_EXPOSURE_TO_THIRD_PARTY_APPS",
    "UNAUTHORIZED_DATA_COLLECTION",
]

ExploitabilityType = Literal["DIFFICULT", "MODERATE", "EASY"]

CompliancePolicyType = Literal["GDPR", "PCI", "HIPAA", "FTC", "OWASP", "CALOPPA", "COPPA", "VPPA", "CCPA", "MITRE"]

PriorityType = Literal["P0", "P1", "P2", "P3", "P4"]

JiraExportType = Literal["ALL_ISSUES", "P1_ISSUES_AND_BLOCKERS", "DISABLED"]

JiraDynamicValueType = Literal["FINDING_PORTAL_URL"]

JiraFieldType = Literal[
    "COMPONENT_ARRAY",
    "NOT_SUPPORTED",
    "NUMBER",
    "OPTION",
    "OPTION_ARRAY_VALUE",
    "STRING",
    "STRING_ARRAY",
    "STRING_MULTISELECT",
    "USER",
]

MobileAppSdkIncludeType = Literal["ios_entitlements", "ios_privacy_manifests"]
