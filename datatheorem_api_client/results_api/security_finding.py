from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.links import ApiResourceLink
from datatheorem_api_client.results_api.types import (
    CategoryType,
    CompliancePolicyType,
    ExploitabilityType,
    ImportanceTagType,
    PriorityType,
    SeverityType,
    StatusType,
)


@dataclass(frozen=True)
class CompliancePolicyReference:
    compliance_policy: CompliancePolicyType
    markdown: str
    link: str

    @classmethod
    def from_dict(cls, compliance_dict: Dict[str, Any]) -> "CompliancePolicyReference":
        return cls(
            compliance_policy=compliance_dict["compliance_policy"],
            markdown=compliance_dict["markdown"],
            link=compliance_dict["link"],
        )


@dataclass(frozen=True)
class Note:
    id: int
    text: str
    author_email: str
    date_created: str
    is_question_for_datatheorem: bool
    is_internal_comment: bool
    is_reply_from_datatheorem: bool

    @classmethod
    def from_dict(cls, note_dict: Dict[str, Any]) -> "Note":
        return cls(
            id=int(note_dict["id"]),
            text=note_dict["text"],
            author_email=note_dict["author_email"],
            date_created=note_dict["date_created"],
            is_question_for_datatheorem=note_dict["is_question_for_datatheorem"],
            is_internal_comment=note_dict["is_internal_comment"],
            is_reply_from_datatheorem=note_dict["is_reply_from_datatheorem"],
        )


@dataclass(frozen=True)
class SecurityFinding:
    """A security or app protection finding found when scanning an app.

    A description of each field is available at https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    cvss_score: float
    cvss_vector: str
    id: str
    date_created: str
    is_permanently_closed: bool
    issue_type_id: str
    title: str
    severity: SeverityType
    category: CategoryType
    exploitability: ExploitabilityType
    description: str
    recommendation: str
    mobile_app_id: int
    importance_tags: List[ImportanceTagType]
    links: List[ApiResourceLink]
    portal_url: str
    aggregated_status: StatusType

    compliance_policy_references: Optional[List[CompliancePolicyReference]] = None
    description_intro: Optional[str] = None
    notes: Optional[List[Note]] = None
    secure_code: Optional[str] = None
    priority: Optional[PriorityType] = None
    results_last_updated: Optional[str] = None
    associated_cve_ids: Optional[List[str]] = None
    associated_cwe_ids: Optional[List[str]] = None

    @classmethod
    def from_dict(cls, finding_dict: Dict[str, Any]) -> "SecurityFinding":
        priority = finding_dict.get("priority", None)
        return cls(
            cvss_score=finding_dict["cvss_score"],
            cvss_vector=finding_dict["cvss_vector"],
            id=finding_dict["id"],
            date_created=finding_dict["date_created"],
            is_permanently_closed=finding_dict["is_permanently_closed"],
            issue_type_id=finding_dict["issue_type_id"],
            title=finding_dict["title"],
            mobile_app_id=int(finding_dict["mobile_app_id"]),
            severity=finding_dict["severity"],
            category=finding_dict["category"],
            exploitability=finding_dict["exploitability"],
            description=finding_dict["description"],
            recommendation=finding_dict["recommendation"],
            portal_url=finding_dict["portal_url"],
            aggregated_status=finding_dict["aggregated_status"],
            importance_tags=[tag for tag in finding_dict.get("importance_tags", [])],
            links=[ApiResourceLink.from_dict(link) for link in finding_dict.get("links", [])],
            compliance_policy_references=[
                CompliancePolicyReference.from_dict(cpr) for cpr in finding_dict.get("compliance_policy_references", [])
            ],
            description_intro=finding_dict.get("description_intro", None),
            notes=[Note.from_dict(note) for note in finding_dict.get("notes", [])],
            secure_code=finding_dict.get("secure_code", None),
            priority=priority or None,
            results_last_updated=finding_dict.get("results_last_updated", None),
            associated_cve_ids=[cve_id for cve_id in finding_dict.get("associated_cve_ids", [])],
            associated_cwe_ids=[cwe_id for cwe_id in finding_dict.get("associated_cwe_ids", [])],
        )
