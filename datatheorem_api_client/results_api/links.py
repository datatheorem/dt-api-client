from dataclasses import dataclass
from typing import Dict, Text


@dataclass(frozen=True)
class ApiResourceLink:
    href: str
    rel: str
    type_: str

    @classmethod
    def from_dict(cls, _dict: Dict[Text, Text]) -> "ApiResourceLink":
        href = _dict["href"]
        rel = _dict["rel"]
        type_ = _dict["type"]

        return cls(href, rel, type_)
