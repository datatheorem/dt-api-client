from dataclasses import dataclass
from typing import Any, Dict, Optional


@dataclass(frozen=True)
class MobileAppScan:
    id: int
    mobile_app_id: int
    status: str
    upload_date: str
    start_date: str
    end_date: Optional[str] = None

    build_version: Optional[str] = None
    most_recently_scanned_build_version: Optional[str] = None

    build_source: Optional[str] = None
    build_source_uploading_user: Optional[str] = None

    release_id: Optional[str] = None
    most_recently_scanned_release_id: Optional[str] = None

    @classmethod
    def from_dict(cls, scan_dict: Dict[str, Any]) -> "MobileAppScan":
        return cls(
            id=scan_dict["id"],
            mobile_app_id=scan_dict["mobile_app_id"],
            status=scan_dict["status"],
            upload_date=scan_dict["upload_date"],
            start_date=scan_dict["start_date"],
            end_date=scan_dict.get("end_date", None),
            build_version=scan_dict.get("build_version", None),
            most_recently_scanned_build_version=scan_dict.get("most_recently_scanned_build_version", None),
            build_source=scan_dict.get("build_source", None),
            build_source_uploading_user=scan_dict.get("build_source_uploading_user", None),
            release_id=scan_dict.get("release_id", None),
            most_recently_scanned_release_id=scan_dict.get("most_recently_scanned_release_id", None),
        )
