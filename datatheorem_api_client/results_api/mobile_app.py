from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.links import ApiResourceLink
from datatheorem_api_client.results_api.types import (
    BlockedScanReasonType,
    PlatformType,
    ReleaseType,
    ScanStatusType,
    SubscriptionType,
)


@dataclass(frozen=True)
class MobileAppAssetTag:
    id: str
    tag: str
    tag_id: str
    value: Optional[str] = None
    imported_from: Optional[str] = None
    imported_external_id: Optional[str] = None

    @classmethod
    def from_dict(cls, tag_dict: Dict[str, Any]) -> "MobileAppAssetTag":
        return cls(
            id=tag_dict["id"],
            tag=tag_dict["tag"],
            tag_id=tag_dict["tag_id"],
            value=tag_dict.get("value", None),
            imported_from=tag_dict.get("imported_from", None),
            imported_external_id=tag_dict.get("imported_external_id", None),
        )


@dataclass(frozen=True)
class MobileApp:
    """A mobile app registered in your organization's Data Theorem account.

    A description of each field is available at
    https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    asset_tags: List[MobileAppAssetTag]
    bundle_id: str
    blocked_scan_reason: BlockedScanReasonType
    has_ci_cd_integration: bool
    has_issue_tracker_integration: bool
    has_trustkit_integration: bool
    id: int
    links: List[ApiResourceLink]
    name: str
    platform: PlatformType
    portal_url: str
    release_type: ReleaseType
    scan_status: ScanStatusType
    subscription: SubscriptionType
    uuid: str
    external_id: Optional[str] = None
    results_last_updated: Optional[str] = None
    scan_status_date: Optional[str] = None
    scanned_release_id: Optional[str] = None
    scanned_version: Optional[str] = None
    store_url: Optional[str] = None

    @classmethod
    def from_dict(cls, app_dict: Dict[str, Any]) -> "MobileApp":
        return cls(
            asset_tags=[MobileAppAssetTag.from_dict(tag) for tag in app_dict.get("asset_tags", [])],
            bundle_id=app_dict["bundle_id"],
            blocked_scan_reason=app_dict["blocked_scan_reason"],
            has_ci_cd_integration=app_dict["has_ci_cd_integration"],
            has_issue_tracker_integration=app_dict["has_issue_tracker_integration"],
            has_trustkit_integration=app_dict["has_trustkit_integration"],
            external_id=app_dict.get("external_id", None),
            id=int(app_dict["id"]),
            links=[ApiResourceLink.from_dict(link) for link in app_dict.get("links", [])],
            name=app_dict["name"],
            platform=app_dict["platform"],
            portal_url=app_dict["portal_url"],
            release_type=app_dict["release_type"],
            results_last_updated=app_dict.get("results_last_updated", None),
            scan_status=app_dict["scan_status"],
            scan_status_date=app_dict.get("scan_status_date", None),
            scanned_release_id=app_dict.get("scanned_released_id", None),
            scanned_version=app_dict.get("scanned_version", None),
            store_url=app_dict.get("store_url", None),
            subscription=app_dict["subscription"],
            uuid=app_dict["uuid"],
        )


@dataclass(frozen=True)
class MobileAppsSubscriptions:
    """Stats for the number of mobile apps for each subscription levels."""

    APP_LOGIC: int
    DYNAMIC: int
    NO_SUBSCRIPTION: int
    P1_ALERTS: int
    STATIC: int

    @classmethod
    def from_dict(cls, app_dict: Dict[str, Any]) -> "MobileAppsSubscriptions":
        return cls(
            APP_LOGIC=app_dict["APP_LOGIC"],
            DYNAMIC=app_dict["DYNAMIC"],
            NO_SUBSCRIPTION=app_dict["NO_SUBSCRIPTION"],
            P1_ALERTS=app_dict["P1_ALERTS"],
            STATIC=app_dict["STATIC"],
        )
