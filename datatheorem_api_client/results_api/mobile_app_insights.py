from dataclasses import dataclass
from typing import Any, Dict, List, Optional

from datatheorem_api_client.results_api.links import ApiResourceLink
from datatheorem_api_client.results_api.types import PlatformType, ReleaseType


@dataclass(frozen=True)
class MobileAppInsightsIosPrivacyManifestSdk:
    id: str
    sdk_in_mobile_app_id: str


@dataclass(frozen=True)
class MobileAppInsightsIosPrivacyManifest:
    file_name: str
    binary_file: str
    manifest_as_json: str
    has_sdk_info: bool
    related_sdks: Optional[List[MobileAppInsightsIosPrivacyManifestSdk]] = None

    @classmethod
    def from_dict(cls, _dict: Dict[str, Any]) -> "MobileAppInsightsIosPrivacyManifest":
        try:
            related_sdks = [
                MobileAppInsightsIosPrivacyManifestSdk(related_sdk["id"], related_sdk["sdk_in_mobile_app_id"])
                for related_sdk in _dict["related_sdks"]
            ]

        except KeyError:
            related_sdks = None

        return cls(
            _dict["file_name"],
            _dict["binary_file"],
            _dict["manifest_as_json"],
            _dict["has_sdk_info"],
            related_sdks,
        )


@dataclass(frozen=True)
class MobileAppInsights:
    """Insights related to an app registered in your organization's Data Theorem account.

    A description of each field is available at https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    mobile_app_id: int
    mobile_app_bundle_id: str
    mobile_app_platform: PlatformType
    mobile_app_release_type: ReleaseType
    portal_url: str
    links: List[ApiResourceLink]
    # Android specific insights
    android_minimum_sdk_version: Optional[str] = None
    android_target_sdk_version: Optional[str] = None
    android_permissions: Optional[List[str]] = None
    android_apk_signature_key_size: Optional[str] = None
    android_exposed_app_activities: Optional[List[str]] = None
    android_exposed_app_content_providers: Optional[List[str]] = None
    android_exposed_app_services: Optional[List[str]] = None
    android_exposed_app_broadcast_receivers: Optional[List[str]] = None
    android_version_name: Optional[str] = None
    android_version_code: Optional[str] = None
    android_native_components_loaded: Optional[List[str]] = None
    android_sha512_hash: Optional[str] = None
    # iOS specific insights
    ios_supports_apple_watch: bool = False
    ios_supports_face_id: bool = False
    ios_supports_imessage: bool = False
    ios_healthkit_accessed: bool = False
    ios_base_sdk: Optional[str] = None
    ios_minimum_sdk: Optional[str] = None
    ios_permissions: Optional[List[str]] = None
    ios_app_extensions: Optional[List[str]] = None
    ios_universal_links_domains: Optional[List[str]] = None
    ios_entitlements: Optional[str] = None
    ios_privacy_manifests: Optional[List[MobileAppInsightsIosPrivacyManifest]] = None
    # Cross-platform insights
    reminders_accessed_by: Optional[List[str]] = None
    microphone_accessed_by: Optional[List[str]] = None
    camera_accessed_by: Optional[List[str]] = None
    calendar_data_accessed_by: Optional[List[str]] = None
    contacts_accessed_by: Optional[List[str]] = None
    photo_library_accessed_by: Optional[List[str]] = None
    location_accessed_by: Optional[List[str]] = None
    advertisement_identifier_accessed_by: Optional[List[str]] = None
    open_server_ports: Optional[List[str]] = None
    app_container_contents: Optional[List[str]] = None
    hard_coded_secrets: Optional[List[str]] = None
    custom_fonts: Optional[List[str]] = None
    # Communication insights
    communicates_with_third_party_origins: Optional[List[str]] = None
    communicates_with_first_party_origins: Optional[List[str]] = None
    communicates_with_ip_addresses: Optional[List[str]] = None
    communicates_with_countries: Optional[List[str]] = None
    communicates_with_blacklisted_ip_addresses: Optional[List[str]] = None

    @classmethod
    def from_dict(cls, insight_dict: Dict[str, Any]) -> "MobileAppInsights":
        try:
            ios_privacy_manifests_dicts = insight_dict["ios_privacy_manifests"]

        except KeyError:
            ios_privacy_manifests = None

        else:
            ios_privacy_manifests = [
                MobileAppInsightsIosPrivacyManifest.from_dict(ios_privacy_manifest)
                for ios_privacy_manifest in ios_privacy_manifests_dicts
            ]

        return cls(
            mobile_app_id=int(insight_dict["mobile_app_id"]),
            mobile_app_bundle_id=insight_dict["mobile_app_bundle_id"],
            mobile_app_platform=insight_dict["mobile_app_platform"],
            mobile_app_release_type=insight_dict["mobile_app_release_type"],
            portal_url=insight_dict["portal_url"],
            links=[ApiResourceLink.from_dict(link) for link in insight_dict.get("links", [])],
            ios_base_sdk=insight_dict.get("ios_base_sdk", None),
            ios_minimum_sdk=insight_dict.get("ios_minimum_sdk", None),
            ios_permissions=insight_dict.get("ios_permissions", []),
            ios_app_extensions=insight_dict.get("ios_app_extensions", []),
            ios_supports_apple_watch=insight_dict.get("ios_supports_apple_watch", False),
            ios_supports_face_id=insight_dict.get("ios_supports_face_id", False),
            ios_supports_imessage=insight_dict.get("ios_supports_imessage", False),
            ios_universal_links_domains=insight_dict.get("ios_universal_links_domains", []),
            ios_healthkit_accessed=insight_dict.get("ios_healthkit_accessed", False),
            ios_entitlements=insight_dict.get("ios_entitlements", None),
            ios_privacy_manifests=ios_privacy_manifests,
            android_minimum_sdk_version=insight_dict.get("android_minimum_sdk_version", None),
            android_target_sdk_version=insight_dict.get("android_target_sdk_version", None),
            android_permissions=insight_dict.get("android_permissions", []),
            android_apk_signature_key_size=insight_dict.get("android_apk_signature_key_size", None),
            android_exposed_app_activities=insight_dict.get("android_exposed_app_activities", []),
            android_exposed_app_content_providers=insight_dict.get("android_exposed_app_content_providers", []),
            android_exposed_app_services=insight_dict.get("android_exposed_app_services", []),
            android_exposed_app_broadcast_receivers=insight_dict.get("android_exposed_app_broadcast_receivers", []),
            android_version_name=insight_dict.get("android_version_name", None),
            android_version_code=insight_dict.get("android_version_code", None),
            android_native_components_loaded=insight_dict.get("android_native_components_loaded", []),
            android_sha512_hash=insight_dict.get("android_sha512_hash", None),
            reminders_accessed_by=insight_dict.get("reminders_accessed_by", []),
            microphone_accessed_by=insight_dict.get("microphone_accessed_by", []),
            camera_accessed_by=insight_dict.get("camera_accessed_by", []),
            calendar_data_accessed_by=insight_dict.get("calendar_data_accessed_by", []),
            contacts_accessed_by=insight_dict.get("contacts_accessed_by", []),
            photo_library_accessed_by=insight_dict.get("photo_library_accessed_by", []),
            location_accessed_by=insight_dict.get("location_accessed_by", []),
            advertisement_identifier_accessed_by=insight_dict.get("advertisement_identifier_accessed_by", []),
            open_server_ports=insight_dict.get("open_server_ports", []),
            app_container_contents=insight_dict.get("app_container_contents", []),
            hard_coded_secrets=insight_dict.get("hard_coded_secrets", []),
            custom_fonts=insight_dict.get("custom_fonts", []),
            communicates_with_third_party_origins=insight_dict.get("communicates_with_third_party_origins", []),
            communicates_with_first_party_origins=insight_dict.get("communicates_with_first_party_origins", []),
            communicates_with_ip_addresses=insight_dict.get("communicates_with_ip_addresses", []),
            communicates_with_countries=insight_dict.get("communicates_with_countries", []),
            communicates_with_blacklisted_ip_addresses=insight_dict.get(
                "communicates_with_blacklisted_ip_addresses", []
            ),
        )
