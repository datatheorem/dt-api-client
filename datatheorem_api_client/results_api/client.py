from __future__ import absolute_import, unicode_literals

from datetime import datetime
from typing import Any, Callable, Dict, List, Optional, Text, Tuple, Union

import requests

from datatheorem_api_client.results_api.jira_integration_config import JiraIntegrationConfig
from datatheorem_api_client.results_api.mobile_app import MobileApp, MobileAppsSubscriptions
from datatheorem_api_client.results_api.mobile_app_insights import MobileAppInsights
from datatheorem_api_client.results_api.mobile_app_scan import MobileAppScan
from datatheorem_api_client.results_api.mobile_app_sdk import MobileAppSdk
from datatheorem_api_client.results_api.sdks import SDK, SDKMobileApp
from datatheorem_api_client.results_api.security_finding import SecurityFinding
from datatheorem_api_client.results_api.security_finding_target import SecurityFindingTarget
from datatheorem_api_client.results_api.types import (
    MobileAppSdkIncludeType,
    PlatformType,
    PriorityType,
    ReleaseType,
    ScanStatusType,
    StatusGroupType,
    StatusType,
    SubscriptionType,
)


class AuthenticationError(Exception):
    pass


class HttpError(Exception):
    pass


PaginationCursor = str


class ResultsAPIClient:
    """API client for accessing Data Theorem's Results API v2, which returns app metadata and app scan results.

    Documentation for the API is available at https://docs.securetheorem.com/mobile_security_results/introduction.html
    """

    RESULTS_API_V2_HOST = "https://api.securetheorem.com"

    API_BASE_PATH = "/apis/mobile_security/results/v2"

    # Paths to the currently supported api resources
    GET_JIRA_INTEGRATION_CONFIG_RESOURCE_PATH = "integration_configs/jira"
    GET_JIRA_INTEGRATION_CONFIG_SPECIFIC_RESOURCE_PATH = "integration_configs/jira/{id}"
    LIST_JIRA_INTEGRATION_CONFIGS = "integration_configs/jira/all"
    LIST_MOBILE_APPS_RESOURCE_PATH = "mobile_apps"
    LIST_SDK_MOBILE_APPS_RESOURCE_PATH = "sdks/{sdk_id}/mobile_apps"
    LIST_SDKS_RESOURCE_PATH = "sdks"
    GET_MOBILE_APPS_SUBSCRIPTIONS_RESOURCE_PATH = "mobile_apps/subscriptions"
    GET_MOBILE_APP_RESOURCE_PATH = "mobile_apps/{id}"
    LIST_SECURITY_FINDINGS_RESOURCE_PATH = "security_findings"
    GET_SECURITY_FINDING_RESOURCE_PATH = "security_findings/{security_finding_id}"
    CREATE_SECURITY_FINDING_NOTE_RESOURCE_PATH = "security_findings/{security_finding_id}/notes"
    LIST_SECURITY_FINDING_TARGETS_RESOURCE_PATH = "security_finding_targets"
    GET_SECURITY_FINDING_TARGETS_RESOURCE_PATH = "security_finding_targets/{security_finding_target_id}"
    PATCH_SECURITY_FINDING_TARGETS_RESOURCE_PATH = "security_finding_targets/{security_finding_target_id}"
    CREATE_SECURITY_FINDING_TARGET_STATUSES_RESOURCE_PATH = (
        "security_finding_targets/{security_finding_target_id}/statuses"
    )
    LIST_MOBILE_APP_INSIGHTS_RESOURCE_PATH = "mobile_app_insights"
    LIST_SDKS_IN_MOBILE_APPS_RESOURCE_PATH = "sdks_in_mobile_apps"
    LIST_MOBILE_APP_SCANS_RESOURCE_PATH = "mobile_apps/{mobile_app_id}/scans"
    GET_MOBILE_APP_SCAN_RESOURCE_PATH = "mobile_apps/{mobile_app_id}/scans/{scan_id}"
    LIST_MOBILE_APP_SDKS_RESOURCE_PATH = "mobile_apps/{mobile_app_id}/sdks"
    GET_MOBILE_APP_SDK_RESOURCE_PATH = "mobile_apps/{mobile_app_id}/sdks/{sdk_id}"

    def __init__(self, api_key: str) -> None:
        self._api_key = api_key

    def get_jira_integration_config(self) -> JiraIntegrationConfig:
        """Fetch the default/global Jira configuration used when integrating with a customer's Jira instance.

        Returns:
            A dict with all information needed to integrate with a customer's Jira instance.
        """
        response = self._call_api(
            self._api_key,
            self.GET_JIRA_INTEGRATION_CONFIG_RESOURCE_PATH,
            method=requests.get,
        )
        return JiraIntegrationConfig.from_dict(response)

    def get_jira_integration_config_specific(self, jira_integration_id: int) -> JiraIntegrationConfig:
        """Fetch the specified Jira configuration used when integration with a customer's Jira instance
        for a specific app.

        Returns:
            A dict with all information needed to integrate with a customer's Jira instance.
        """
        response = self._call_api(
            self._api_key,
            self.GET_JIRA_INTEGRATION_CONFIG_SPECIFIC_RESOURCE_PATH.format(jira_integration_id),
            method=requests.get,
        )
        return JiraIntegrationConfig.from_dict(response)

    def list_jira_integration_configs(self) -> List[JiraIntegrationConfig]:
        """List all jira integration configs from within a customer

        Returns:
            A list of all `JiraIntegrationConfig` objects within a customer
        """
        response = self._call_api(self._api_key, self.LIST_JIRA_INTEGRATION_CONFIGS, method=requests.get)

        return [JiraIntegrationConfig.from_dict(config) for config in response["jira_integrations"]]

    def list_mobile_apps(
        self,
        cursor: Optional[PaginationCursor] = None,
        external_id: Optional[str] = None,
        platform: Optional[PlatformType] = None,
        release_type: Optional[ReleaseType] = None,
        results_since: Optional[datetime] = None,
        scan_status: Optional[ScanStatusType] = None,
        subscription: Optional[Union[SubscriptionType, List[SubscriptionType]]] = None,
    ) -> Tuple[List[MobileApp], Optional[PaginationCursor]]:
        """List of all apps within a customer.

        Args:
            cursor: A cursor for fetching paginated results.
            external_id: Filters the list of apps by external ID.
            platform: Filters the list of apps by a platform value.
            release_type: Filters the list of apps by a release type value.
            results_since: Filters the list of apps based upon when they last had activity due to
            scans.
            scan_status: Filters the list of apps by the status of the app's most
            recent scan value.
            subscription: Filters the list of apps by subscription level value.

        Returns:
            A list of `MobileApp` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "external_id": external_id,
                "platform": platform,
                "release_type": release_type,
                "results_since": results_since.isoformat() if results_since else None,
                "scan_status": scan_status,
                "subscription": subscription,
            }
        )
        mobile_apps_resource = self.LIST_MOBILE_APPS_RESOURCE_PATH
        response = self._call_api(self._api_key, mobile_apps_resource, params=params, method=requests.get)

        # It's possible to have an empty page with a next_cursor so we always try to parse the cursor
        mobile_apps = [MobileApp.from_dict(app_dict) for app_dict in response.get("mobile_apps", [])]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return mobile_apps, next_cursor

    def list_sdk_mobile_apps(
        self,
        sdk_id: str,
        cursor: Optional[PaginationCursor] = None,
    ) -> Tuple[List[SDKMobileApp], Optional[PaginationCursor]]:
        """List the mobile apps using a given SDK.

        Returns:
            A list of `SDK` and a pagination cursor.
        """
        params = self._get_optional_param_values({"cursor": cursor})
        sdks_resource = self.LIST_SDK_MOBILE_APPS_RESOURCE_PATH.format(sdk_id=sdk_id)
        response = self._call_api(self._api_key, sdks_resource, params=params, method=requests.get)

        # It's possible to have an empty page with a next_cursor so we always try to parse the cursor
        sdks = [SDKMobileApp.from_dict(app_dict) for app_dict in response.get("mobile_apps", [])]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return sdks, next_cursor

    def list_sdks(
        self,
        cursor: Optional[PaginationCursor] = None,
    ) -> Tuple[List[SDK], Optional[PaginationCursor]]:
        """List the known SDKs.

        Returns:
            A list of `SDK` and a pagination cursor.
        """
        params = self._get_optional_param_values({"cursor": cursor})
        sdks_resource = self.LIST_SDKS_RESOURCE_PATH
        response = self._call_api(self._api_key, sdks_resource, params=params, method=requests.get)

        # It's possible to have an empty page with a next_cursor so we always try to parse the cursor
        sdks = [SDK.from_dict(sdk_dict) for sdk_dict in response.get("sdks", [])]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return sdks, next_cursor

    def get_mobile_app_subscriptions(self) -> MobileAppsSubscriptions:
        """Get the mobile app stats for each subscriptions.

        Returns:
            A `MobileAppsSubscriptions`.
        """
        mobile_apps_subscriptions_resource = self.GET_MOBILE_APPS_SUBSCRIPTIONS_RESOURCE_PATH
        response = self._call_api(self._api_key, mobile_apps_subscriptions_resource, method=requests.get)
        return MobileAppsSubscriptions.from_dict(response)

    def get_mobile_app(self, mobile_app_id: int) -> MobileApp:
        """Get a specific app within a customer by app id.

        Args:
            mobile_app_id: The numerical app id.

        Returns:
            A `MobileApp`.
        """
        mobile_apps_resource = self.GET_MOBILE_APP_RESOURCE_PATH.format(id=mobile_app_id)
        response = self._call_api(self._api_key, mobile_apps_resource, method=requests.get)
        return MobileApp.from_dict(response)

    def list_security_findings(
        self,
        cursor: Optional[PaginationCursor] = None,
        mobile_app_id: Optional[int] = None,
        results_since: Optional[datetime] = None,
        status_group: Optional[StatusGroupType] = None,
    ) -> Tuple[List[SecurityFinding], Optional[PaginationCursor]]:
        """List of all issues for a given customer's apps.

        Args:
            cursor: A cursor for fetching paginated results.
            mobile_app_id: Filters the list of issues related to an app by id.
            results_since: Filters the list of issues based upon when they last had activity due
            to scans.
            status_group: Filters the list of issues based on their status group.

        Returns:
            A list of `SecurityFinding` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "mobile_app_id": mobile_app_id,
                "results_since": results_since.isoformat() if results_since else None,
                "status_group": status_group,
            }
        )
        security_findings_resource = self.LIST_SECURITY_FINDINGS_RESOURCE_PATH
        response = self._call_api(
            self._api_key,
            security_findings_resource,
            params=params,
            method=requests.get,
        )

        # It's possible to have an empty page with a next_cursor so we always try to parse the cursor
        findings = [SecurityFinding.from_dict(finding_dict) for finding_dict in response.get("security_findings", [])]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return findings, next_cursor

    def get_security_finding(self, security_finding_id: str) -> SecurityFinding:
        """Get a specific issue for an app within a customer.

        Args:
            security_finding_id: The string security finding id.

        Returns:
            A `SecurityFinding`.
        """
        security_findings_resource = self.GET_SECURITY_FINDING_RESOURCE_PATH.format(
            security_finding_id=security_finding_id
        )
        response = self._call_api(self._api_key, security_findings_resource, method=requests.get)
        return SecurityFinding.from_dict(response)

    def patch_security_finding(
        self,
        security_finding_id: str,
        priority: Optional[PriorityType] = None,
    ) -> SecurityFinding:
        """Modify a specific issue for an app within a customer.

        Args:
            security_finding_id: The string security finding id.
            priority: The new priority of the security finding.

        Returns:
            A `SecurityFinding`.
        """
        payload = self._get_optional_param_values({"priority": priority})
        security_findings_resource = self.GET_SECURITY_FINDING_RESOURCE_PATH.format(
            security_finding_id=security_finding_id
        )
        response = self._call_api(
            self._api_key,
            security_findings_resource,
            method=requests.patch,
            payload=payload,
        )
        return SecurityFinding.from_dict(response)

    def create_security_finding_note(
        self,
        security_finding_id: str,
        text: str,
        is_question_for_datatheorem: Optional[bool] = None,
    ) -> None:
        """Create a comment on the security finding. Restrictions apply based on the API key's permissions (see
        https://docs.securetheorem.com/mobile_security_results/security_findings.html#Post-Comment-on-Security-Finding
        for details).

        The method returns nothing since the endpoint returns with 204 (No Content) status.

        Args:
            security_finding_id: The string security finding ID
            text: The contents of the comment
            is_question_for_datatheorem: Whether the comment is a question for Data Theorem, or an internal comment
        """
        payload = self._get_optional_param_values(
            {
                "text": text,
                "is_question_for_datatheorem": is_question_for_datatheorem,
            }
        )

        security_finding_targets_resource = self.CREATE_SECURITY_FINDING_NOTE_RESOURCE_PATH.format(
            security_finding_id=security_finding_id
        )
        self._call_api(self._api_key, security_finding_targets_resource, payload=payload, method=requests.post)

    def list_security_finding_targets(
        self,
        cursor: Optional[PaginationCursor] = None,
        mobile_app_id: Optional[int] = None,
        results_since: Optional[datetime] = None,
        security_finding_id: Optional[Text] = None,
        status_group: Optional[StatusGroupType] = None,
    ) -> Tuple[List[SecurityFindingTarget], Optional[PaginationCursor]]:
        """List of all instances of issues within a customer's apps.

        Args:
            cursor : A cursor for fetching paginated results.
            mobile_app_id: Filters the list of targets related to the app by id.
            results_since: Filters the list of targets based upon when they last had activity due
            to scans.
            security_finding_id : Filters the list of targets based on the security finding by id.
            status_group: Filters the list of targets based on their current status group.

        Returns:
            A list of `SecurityFindingTarget` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "mobile_app_id": mobile_app_id,
                "results_since": results_since.isoformat() if results_since else None,
                "security_finding_id": security_finding_id,
                "status_group": status_group,
            }
        )
        security_findings_targets_resource = self.LIST_SECURITY_FINDING_TARGETS_RESOURCE_PATH
        response = self._call_api(
            self._api_key,
            security_findings_targets_resource,
            params=params,
            method=requests.get,
        )

        # It's possible to have an empty page with a next_cursor so we always try to parse the cursor
        targets = [
            SecurityFindingTarget.from_dict(target_dict) for target_dict in response.get("security_finding_targets", [])
        ]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return targets, next_cursor

    def get_security_finding_target(self, security_finding_target_id: int) -> SecurityFindingTarget:
        """Get a specific target for an issue within a customer's app.

        Args:
            security_finding_target_id: The numerical security finding target id

        Returns:
             A `SecurityFindingTarget`.
        """
        security_findings_targets_resource = self.GET_SECURITY_FINDING_TARGETS_RESOURCE_PATH.format(
            security_finding_target_id=security_finding_target_id
        )
        response = self._call_api(self._api_key, security_findings_targets_resource, method=requests.get)
        return SecurityFindingTarget.from_dict(response)

    def patch_security_finding_target(self, security_finding_target_id: int, external_id: str) -> SecurityFindingTarget:
        """Updates the `external_id` field for an instance of an issue. Restrictions apply based on the
        API key's permissions (see
        https://docs.securetheorem.com/mobile_security_results/security_finding_targets.html#Patch-Security-Finding-Target
        for details).

        Args:
            security_finding_target_id: The numerical security finding target id

            external_id: A string representation of an external id that's assigned to the issue possibly from an
            external bug tracking system such as Jira.

        Returns:
             A `SecurityFindingTarget`.
        """
        payload = {"external_id": external_id}
        security_finding_targets_resource = self.PATCH_SECURITY_FINDING_TARGETS_RESOURCE_PATH.format(
            security_finding_target_id=security_finding_target_id
        )
        resp = self._call_api(
            self._api_key,
            security_finding_targets_resource,
            payload=payload,
            method=requests.patch,
        )
        return SecurityFindingTarget.from_dict(resp)

    def create_security_finding_target_status(self, security_finding_target_id: int, status: StatusType) -> None:
        """Create new status to override the current status of a security finding target.
        Restrictions apply based on the current status and the API key's permissions (see
        https://docs.securetheorem.com/mobile_security_results/security_finding_targets.html#Update-Status-on-Security-Finding-Target
        for details).

        The method returns nothing since the endpoint returns with 204 (No Content) status.

        Args:
            security_finding_target_id: The numerical security finding target ID
            status: The new status.
        """
        payload = {"status": status}
        security_finding_targets_resource = self.CREATE_SECURITY_FINDING_TARGET_STATUSES_RESOURCE_PATH.format(
            security_finding_target_id=security_finding_target_id
        )
        self._call_api(self._api_key, security_finding_targets_resource, payload=payload, method=requests.post)

    def list_mobile_app_insights(
        self,
        cursor: Optional[PaginationCursor] = None,
        mobile_app_platform: Optional[PlatformType] = None,
        mobile_app_release_type: Optional[ReleaseType] = None,
        mobile_app_bundle_id: Optional[Text] = None,
        include: Optional[List[MobileAppSdkIncludeType]] = None,
    ) -> Tuple[List[MobileAppInsights], Optional[PaginationCursor]]:
        """List of mobile app insights for all apps within a customer.

        Args:
            cursor: A cursor for fetching paginated results.
            mobile_app_platform: Filters the list of app insights by a platform value.
            mobile_app_release_type: Filters the list of app insights by a release type
            value.
            mobile_app_bundle_id (Text, optional): Filters the list of app insights to apps with the given bundle id.
            This query parameter should be used in combination with the `mobile_app_platform` and
            `mobile_app_release_type` in order to filter down to a specific mobile app.
            include: A set of extra fields to include in the response.

        Returns:
            A list of `MobileAppInsights` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "mobile_app_bundle_id": mobile_app_bundle_id,
                "mobile_app_platform": mobile_app_platform,
                "mobile_app_release_type": mobile_app_release_type,
                "include": include,
            }
        )
        mobile_app_insights_resource = self.LIST_MOBILE_APP_INSIGHTS_RESOURCE_PATH
        response = self._call_api(
            self._api_key,
            mobile_app_insights_resource,
            params=params,
            method=requests.get,
        )

        if "mobile_app_insights" not in response.keys():
            return [], None

        mobile_app_insights = [
            MobileAppInsights.from_dict(app_insight_dict) for app_insight_dict in response["mobile_app_insights"]
        ]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return mobile_app_insights, next_cursor

    def list_sdks_in_mobile_apps(
        self,
        cursor: Optional[PaginationCursor] = None,
        mobile_app_id: Optional[int] = None,
        sdk_id: Optional[str] = None,
        is_currently_present: Optional[bool] = None,
    ) -> Tuple[List[MobileAppSdk], Optional[PaginationCursor]]:
        """List of mobile app third-party SDKs for all apps within a customer.

        Args:
            cursor: A cursor for fetching paginated results.
            mobile_app_id: Filters the list of targets related to the app by id.
            sdk_id: Filters the list of third-party SDKs issues based on the SDK id.
            is_currently_present: Filters the list of third-party SDKs based on the presence of the SDK during the most
             recent scan.

        Returns:
            A list of `MobileAppSdk` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "mobile_app_id": mobile_app_id,
                "sdk_id": sdk_id,
                "currently_present": is_currently_present,
            }
        )
        mobile_app_sdks_resource = self.LIST_SDKS_IN_MOBILE_APPS_RESOURCE_PATH
        response = self._call_api(
            self._api_key,
            mobile_app_sdks_resource,
            params=params,
            method=requests.get,
        )

        if "sdks_in_mobile_apps" not in response.keys():
            return [], None

        mobile_app_sdks = [
            MobileAppSdk.from_dict(app_insight_dict) for app_insight_dict in response["sdks_in_mobile_apps"]
        ]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return mobile_app_sdks, next_cursor

    def list_mobile_app_sdks(
        self,
        mobile_app_id: int,
        cursor: Optional[PaginationCursor] = None,
        sdk_id: Optional[str] = None,
        is_currently_present: Optional[bool] = None,
    ) -> Tuple[List[MobileAppSdk], Optional[PaginationCursor]]:
        """List of mobile app third-party SDKs for an App within a customer.

        Args:
            mobile_app_id: App id.
            cursor: A cursor for fetching paginated results.
            sdk_id: Filters the list of third-party SDKs issues based on the SDK id.
            is_currently_present: Filters the list of third-party SDKs based on the presence of the SDK during the most
             recent scan.

        Returns:
            A list of `MobileAppSdk` objects and a cursor if more data is available.
        """
        params = self._get_optional_param_values(
            {
                "cursor": cursor,
                "sdk_id": sdk_id,
                "currently_present": is_currently_present,
            }
        )
        mobile_app_sdks_resource = self.LIST_MOBILE_APP_SDKS_RESOURCE_PATH.format(mobile_app_id=mobile_app_id)
        response = self._call_api(
            self._api_key,
            mobile_app_sdks_resource,
            params=params,
            method=requests.get,
        )

        mobile_app_sdks = [
            MobileAppSdk.from_dict(app_insight_dict) for app_insight_dict in response.get("sdks_in_mobile_app", [])
        ]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return mobile_app_sdks, next_cursor

    def get_mobile_app_sdk(self, mobile_app_id: int, sdk_id: str) -> MobileAppSdk:
        """Fetch a mobile app third-party SDK.

        Args:
            mobile_app_id: the app by id.
            sdk_id: the third-party SDK id.

        Returns:
            A `MobileAppSdk` object.
        """
        mobile_app_sdks_resource = self.GET_MOBILE_APP_SDK_RESOURCE_PATH.format(
            mobile_app_id=mobile_app_id, sdk_id=sdk_id
        )
        response = self._call_api(
            self._api_key,
            mobile_app_sdks_resource,
            method=requests.get,
        )

        return MobileAppSdk.from_dict(response)

    def list_mobile_app_scans(
        self, mobile_app_id: int, cursor: Optional[str] = None
    ) -> Tuple[List[MobileAppScan], Optional[PaginationCursor]]:
        """List scans of a specific mobile app

        Args:
            mobile_app_id: A mobile apps ID
            cursor : A cursor for fetching paginated results.

        Returns:
            A list of `MobileAppScan` objects and a cursor if more data is available
        """
        params = self._get_optional_param_values({"cursor": cursor})
        scans_resource = self.LIST_MOBILE_APP_SCANS_RESOURCE_PATH.format(mobile_app_id=mobile_app_id)
        response = self._call_api(self._api_key, scans_resource, params=params, method=requests.get)

        if "scans" not in response.keys():
            return (
                [],
                None,
            )

        scans = [MobileAppScan.from_dict(scan) for scan in response["scans"]]
        next_cursor = response["pagination_information"].get("next_cursor", None)
        return scans, next_cursor

    def get_mobile_app_scan(self, mobile_app_id: int, scan_id: int) -> MobileAppScan:
        """Get a specific scan
        Args:
            mobile_app_id: A mobile app ID
            scan_id: The ID of the scan to retrive
        Returns:
            A `MobileAppScan` object
        """
        scan_resource = self.GET_MOBILE_APP_SCAN_RESOURCE_PATH.format(mobile_app_id=mobile_app_id, scan_id=scan_id)
        response = self._call_api(self._api_key, scan_resource, method=requests.get)
        return MobileAppScan.from_dict(response)

    def _call_api(
        self,
        api_key: str,
        api_resource_path: str,
        method: Callable,
        params: Optional[Dict[Any, Any]] = None,
        payload: Optional[Dict[Any, Any]] = None,
    ) -> Dict[Text, Any]:
        """Helper function for calling Results API v2 resources."""
        api_url = "{}{}/{}".format(self.RESULTS_API_V2_HOST, self.API_BASE_PATH, api_resource_path)
        auth_header = {"Authorization": "APIKey {}".format(api_key)}

        # Query parameters to pass to the request if any
        if params:
            response = method(api_url, params=params, headers=auth_header)
        elif payload:
            response = method(api_url, json=payload, headers=auth_header)
        else:
            response = method(api_url, headers=auth_header)

        if response.status_code == 401:
            raise AuthenticationError("Missing or invalid API key")

        if response.status_code >= 400:
            raise HttpError(response.content)

        if response.status_code == 204:
            return {}

        return response.json()

    def _get_optional_param_values(self, params: Dict[str, Optional[Any]]) -> Dict[str, Any]:
        non_none_params: Dict[str, Any] = {}
        non_none_params.update((k, v) for k, v in list(params.items()) if v is not None)
        return non_none_params
