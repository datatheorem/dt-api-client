# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import csv

from argparse import ArgumentParser

from cvss_plugin import CvssPlugin
from datatheorem_api_client.results_api.client import ResultsAPIClient
import sys

if sys.version_info[0] < 3:
    IS_PY_2 = True
else:
    IS_PY_2 = False


def get_status_date_opened(statuses: list) -> str:
    statuses = sorted(statuses, key=lambda status: status.date)
    return statuses[0].date


def main() -> None:
    parser = ArgumentParser(
        description="Results API v2 CLI tool to export all issue to a CSV file."
    )
    parser.add_argument("--api-key", help="Your Results API key.")
    args = parser.parse_args()

    # Validate arguments
    api_key = args.api_key
    if not api_key:
        sys.exit("Results API key missing; set it using `--api-key`")

    # Initialize the API client
    api_client = ResultsAPIClient(api_key)

    # Fetch all targets
    security_finding_targets, next_cursor = api_client.list_security_finding_targets()
    # Keep on fetching while we have more results
    while next_cursor:
        print("Fetching Targets...")
        next_targets, next_cursor = api_client.list_security_finding_targets(
            cursor=next_cursor
        )
        security_finding_targets.extend(next_targets)

    # Fetch all findings
    security_findings, next_cursor = api_client.list_security_findings()
    # Keep on fetching while we have more results
    while next_cursor:
        print("Fetching Findings...")
        next_findings, next_cursor = api_client.list_security_findings(
            cursor=next_cursor
        )
        security_findings.extend(next_findings)

    security_findings_dict = {finding.id: finding for finding in security_findings}

    # Fetch all the apps
    mobile_apps, next_cursor = api_client.list_mobile_apps()
    # Keep on fetching while we have more results
    while next_cursor:
        print("Fetching Mobile Apps...")
        next_apps, next_cursor = api_client.list_mobile_apps(cursor=next_cursor)
        mobile_apps.extend(next_apps)

    mobile_apps_dict = {app.id: app for app in mobile_apps}

    # Write everything to a CSV file
    output_filename = "output.csv"
    print("Writing CSV to {}....".format(output_filename))
    with open(output_filename, "w") as csvfile:
        fields = [
            "title",
            "App Name",
            "App Platform",
            "App Bundle ID",
            "category",
            "priority",
            "severity",
            "exploitability",
            "Security P1",
            "Google Play Blocker",
            "App Store Blocker",
            "affected_component",
            "description",
            "recommendation",
            "secure_code",
            "id",
            "date_opened",
            "current_status",
            "current_status_date",
            "mobile_app_id",
            "security_finding_id",
            "portal_url",
            "cvss",
        ]
        writer = csv.writer(csvfile)
        if IS_PY_2:
            fields = [f.encode("utf-8") for f in fields]
        writer.writerow(fields)

        for target in security_finding_targets:
            # Retrieve the corresponding finding and app
            finding = security_findings_dict[target.security_finding_id]
            mobile_app = mobile_apps_dict[finding.mobile_app_id]

            has_security_p1_tag = (
                True if "SECURITY_P1" in finding.importance_tags else False
            )
            has_google_blocker_tag = (
                True if "GOOGLE_P1" in finding.importance_tags else False
            )
            has_app_store_blocker_tag = (
                True if "APPLE_P1" in finding.importance_tags else False
            )

            row = [
                finding.title,
                mobile_app.name,
                mobile_app.platform,
                mobile_app.bundle_id,
                finding.category,
                finding.priority if finding.priority else "",
                finding.severity,
                finding.exploitability,
                str(has_security_p1_tag),
                str(has_google_blocker_tag),
                str(has_app_store_blocker_tag),
                target.text,
                finding.description,
                finding.recommendation,
                finding.secure_code if finding.secure_code else "",
                target.id,
                get_status_date_opened(target.statuses),
                target.current_status,
                target.current_status_date,
                target.mobile_app_id,
                target.security_finding_id,
                target.portal_url,
                str(CvssPlugin.compute_cvss_score(finding)),
            ]

            if IS_PY_2:
                row = [r.encode("utf-8") for r in row]
            writer.writerow(row)

    print("All done... all issues were written to ./{}....".format(output_filename))


if __name__ == "__main__":
    main()
